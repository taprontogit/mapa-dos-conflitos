<?php
get_header(); 
?>

<main class="institucional">
    <div class="container-fluid wrap">
        <h2>Reports</h2>
        <div class="row acenter">
            <div class="col-md-6 col-xs-12 mb2">
                <a href="https://apublica.org/2022/04/assassinatos-no-campo-em-2021-batem-recorde-dos-ultimos-quatro-anos/" target="_blank" class="reportagem">
                    <div class="zoomimg"><img src="<?php tu(); ?>/assets/images/r1.jpg"></div>
                    <h3>Countryside killings in 2021 break four-year record</h3>
                    <p>Under Bolsonaro, the average number of conflicts is already the highest in history. Last year, 35 people were murdered in the countryside, 29 in the Amazon alone.</p>
                </a>
                <a href="https://apublica.org/2022/04/assassinatos-no-campo-em-2021-batem-recorde-dos-ultimos-quatro-anos/" target="_blank" class="btn">read report</a>
            </div>
            <div class="col-md-6 col-xs-12 mb2">
                <a href="https://apublica.org/2022/04/mais-de-cem-mil-familias-foram-afetadas-em-uma-decada-de-conflitos-no-campo-na-amazonia/" target="_blank" class="reportagem">
                    <div class="zoomimg"><img src="<?php tu(); ?>/assets/images/r2.jpg"></div>
                    <h3 class="pb1">More than 100 families have been affected in a decade of conflict in the Amazon countryside</h3>
                    <p>Map of conflicts, exclusive tool launched by Agência Pública and CPT, maps conflicts and socio-environmental data from all municipalities in the Legal Amazon</p>
                </a>
                <a href="https://apublica.org/2022/04/mais-de-cem-mil-familias-foram-afetadas-em-uma-decada-de-conflitos-no-campo-na-amazonia/" target="_blank" class="btn">read report</a>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>