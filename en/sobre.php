<?php
get_header(); 
?>

<main class="institucional sobre">
    <div class="container-fluid wrap">
        <h2>About</h2>
        <p>The Map of Conflict is a project by the investigative journalism outlet <strong>Ag&ecirc;ncia P&uacute;blica</strong>, in partnership with the <strong>Pastoral Land Commission (CPT)</strong>. Using original data analysis, it investigates instances of rural conflict in Brazil&rsquo;s Legal Amazon region in the last decade (2011-2020).</p>
        <p>In this special project, rural conflicts were analysed alongside public data on other socioenvironmental issues, such as deforestation, wildfires, mining, water, agrotoxins, violence, and inequality.</p>
        <p>These instances of rural conflict are recorded and published in the annual &lsquo;Conflitos no Campo&rsquo; reports by the CPT. A pastoral action of the Catholic Church, the CPT is the only organisation which has carried out such extensive research on rural conflict in Brazil.</p>
        <p>The term &lsquo;rural conflict&rsquo; refers to instances of confrontation and resistance occurring in different social contexts in the countryside. They involve struggles for land, water, rights, and the means of work or production; and occur between different social classes, amongst workers, because of an absence of public policy, or where policy has been poorly implemented.</p>
        <p>This decades-long task has generated one of the most important documental archives on territorial struggles and on the forms of resistance of those who work on the land, on the water and in the forests, as well as on the attainment and defence of rights.</p>
        <p><strong>Ag&ecirc;ncia P&uacute;blica</strong>, which was created by female reporters in 2011,is Brazil&rsquo;s first non-profit investigative journalism agency. Our investigative reports are determined by public interest and based on rigorous establishment of the facts and the unconditional defence of human rights. According to the ranking by Jornalistas &amp; Cia, it has received more awards than any other Brazilian news agency.</p>
        <p>For 11 years we have been investigating the issue of land ownership in the Amazon and the fight of indigenous peoples, riverine communities, and small-scale farmers against the plunder of their land and the displacement of their communities.</p>
        <p>The Map of Conflict is part of the special &lsquo;The Lawless Amazon&rsquo;, which investigates violence related to landholding registration, demarcation of land for indigenous groups and traditional communities, and agrarian reform in the Legal Amazon and in the Cerrado.</p>
    
        <div class="contato">
            <p class="w700">Contact: </p>
            <p>For more information, contact us using the <a href="https://apublica.org/contato/" target="_blank">online form</a></p>
            <p>e-mail: <a href="mailto:redacao@apublica.org" target="_blank">redação@apublica.org</a></p>
        </div>
    </div>
</main>

<?php get_footer(); ?>