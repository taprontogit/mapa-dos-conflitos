<?php
$current_page = 'home';
get_header(); 
?>

<main id="intro">
    <div class="intro-section intro-1">
        <div class="center">
            <div class="logoglitch"><img src="<?php tu(); ?>/assets/images/main-logo.svg" alt="Mapa dos Conflitos"><img class="lglitch" src="<?php tu(); ?>/assets/images/main-logo-c.svg" alt="Mapa dos Conflitos"></div>
            <p>Map of Conflict – a decade of violence and land injustice in Brazil’s Legal Amazon</p>
        </div>
        <div class="apoiadores">
            <img src="<?php tu(); ?>/assets/images/publica.png">
            <img src="<?php tu(); ?>/assets/images/cpt.png">
        </div>
    </div>
    <div class="intro-section intro-2">
        <div class="center">
            <div>
                <h2>The Legal Amazon</h2>
                <p>The Map of Conflict focuses on Brazil’s ‘Legal Amazon’ region, an area covering over five million square kilometres, two-thirds of the country. This includes the states of Amazonas, Roraima, Rondônia, Pará, Amapá, Acre, Tocantins, Mato Grosso, and part of Maranhão. The Legal Amazon was the location of 55% of land conflicts in Brazil between 2011 and 2020.</p>
            </div>
        </div>
    </div>
    <div class="intro-section intro-big">
        <div class="center">
            <div>
                <div class="frases">
                    <h2>The last decade in the Amazon</h2>
                    <span class="data">(2011-2020)</span>
                    <h4><strong>+ than 300</strong> killings</h4>
                    <h4><strong>+ than 2.000</strong> victims</h4>
                    <h4><strong>+ than 100.000</strong> families affected</h4>
                    <h4><strong>+ than 7.000</strong> instances of conflict</h4>
                    <h4><strong>9</strong> states e <strong>583</strong> municipalities affected</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-section conflitos-section">
        <div class="center">
            <div style="overflow-x: hidden;">
                <?php get_template_part('blocks/_temas-intro'); ?>
                <div class="fixpos">
                    <div class="infos-geral">
                        <h2>Map of Conflict</h2>
                        <p>Browse using the filters and see how rural conflicts are linked to deforestation, wildfires, mining, water, agrotoxins, violence, and inequality. See below what each filter represents.</p>
                    </div>
                    <div class="infos-tema" data-indicador="1">
                        <h2>Conflicts</h2>
                        <p>The occurrences of rural conflicts are collected annually by the CPT and systematized by the Dom Tomás Balduino Documentation Center – CEDOC.</p>
                    </div>
                    <div class="infos-tema" data-indicador="6">
                        <h2>Wildfires</h2>
                        <p>The fires are measured by INPE. This filter shows the number of fires per municipality, adjusted by land area.</p>
                    </div>
                    <div class="infos-tema" data-indicador="4">
                        <h2>Deforestation</h2>
                        <p>The higher the score, the greater the deforestation in the municipality, as measured by the National Institute for Space Research (INPE). The score is adjusted for the land area of the municipality.</p>
                    </div>
                    <div class="infos-tema" data-indicador="2">
                        <h2>Agrotoxins</h2>
                        <p>The filter shows the number of recorded cases of poisoning due to toxins used in agriculture. Poisoning may be caused by accidents in the handling of these chemicals, in a work setting, or as a result of spraying.</p>
                    </div>
                    <div class="infos-tema" data-indicador="8">
                        <h2>Inequality</h2>
                        <p>Here we compare data which show the quality of life of the population, based on the Municipal Human Development Index (IDH-M): life expectancy, years of schooling, and <i>per capita</i> income.</p>
                    </div>
                    <div class="infos-tema" data-indicador="3">
                        <h2>Water</h2>
                        <p>To use water from rivers, lakes, and other sources for purposes of agriculture and industry, concessions must be obtained from government. This filter shows the number of concessions granted for water use by municipality.</p>
                    </div>
                    <div class="infos-tema" data-indicador="5">
                        <h2>Mining</h2>
                        <p>To carry out legal mining operations, requests are filed with the National Mining Agency (ANM). This filter shows the number of these requests by municipality, adjusted for land area.</p>
                    </div>
                    <div class="infos-tema" data-indicador="7">
                        <h2>Violence</h2>
                        <p>The filter shows the number of hospitalizations due to violence of various kinds. The score is adjusted for the size of the municipal population.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-section fim">
    </div>

    <div class="scrollTo"><span></span></div>
    <a href="<?php echo home_url('mapa'); ?>" class="btn">skip intro</a>
    <div class="vinheta"></div>
</main>

<?php get_footer(); ?>