<?php
get_header(); 
?>

<main class="institucional animacao">
    <div class="container-fluid wrap">
        <h2>Animation</h2>
        <div class="video-wrapper">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/ECBcFxPeKww" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="mt2">
            <p class="w700 mb0">Land conflicts in five minutes</p>
            <p class="mt0"><i>Narrated by award-winning writer Itamar Vieira Junior</i></p>
            <p>In this animation we invite you to reflect on the relationship between soya and cattle farming and violence; how agribusiness, mining, and energy generation may be linked to violent conflicts in the countryside; and how poor policymaking underpins all these issues, issues which have led hundreds of activists such as Dorothy Stang and Chico Mendes to die in defence of the forests.</p>
        </div>
    </div>
</main>

<?php get_footer(); ?>