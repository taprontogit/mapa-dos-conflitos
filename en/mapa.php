<?php
$current_page = 'mapa';
$topStyle = 'temas';
$titulo = 'Mapa dos Conflitos';
get_header(); 
?>

<main id="mainControle">

    <div id="map"></div>

    <!-- elementos com posição variável -->
    <div id="tooltipmap" class="tooltip bottom"></div>

    <a class="btn-goto" id="iniciarmapa" onclick="mapApp.indicator=1;mapApp.filter();">ir para o mapa</a>

    <div class="map-controllers">
        <div class="zoom">
            <button class="more" onclick="play('click');if(mapApp.map)mapApp.map.setZoom(mapApp.map.getZoom()+1)"></button>
            <button class="less" onclick="play('click');if(mapApp.map)mapApp.map.setZoom(mapApp.map.getZoom()-1)"></button>
            <button class="center" onclick="play('click');if(mapApp.map)mapApp.zoom(false)"></button>
        </div>

        <div class="infos">
            <a href="<?php echo home_url('metodologia'); ?>" class="faq"></a>
            <div class="share-wrapper" onmouseenter="play('hover');">
                <div class="share-btns">
                    <a class="twitter" target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo urlencode($titulo); ?>&amp;url=<?php echo urlencode(currentURL()); ?>"></a>
                    <a class="facebook" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(currentURL()); ?>&amp;t=<?php echo urlencode($titulo); ?>"></a>
                    <a class="linkedin" target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo urlencode(currentURL()); ?>"></a>
                    <a class="whatsapp" target="_blank" href="https://wa.me/?text=<?php echo urlencode(currentURL()); ?>"></a>
                </div>
                <a class="share"></a>
            </div>
        </div>

        <div class="regua">
            <div class="regua-inner">
                <div class="legenda-regua">
                    <p>mín.</p>
                    <p class="titulo" id="escalalabel">Conflicts</p>
                    <p>max.</p>
                </div>
                <div class="escala" id="escalamapa"></div>
                <div class="tooltip bottom right">
                    <p class="label">Each county color varies according to the number of occurrences by the selected lens — the darker the color, the higher the value. The data are explained in the methodology.</p>
                </div>
            </div>
        </div>

        <div class="card" id="mapcard">
            <button class="btn-close" onclick="play('close');this.parentNode.classList.remove('active');mapApp.zoom();"></button>
            <div id="cardcontent">
            </div>
        </div>
    </div>
</main>
<span class="nuvem" style="top:<?php echo rand(12,50); ?>%; left: <?php echo rand(10,90); ?>%;"></span>
<span class="nuvem c2" style="top:<?php echo rand(50,85); ?>%; left: <?php echo rand(10,90); ?>%;"></span>
<?php get_footer(); ?>