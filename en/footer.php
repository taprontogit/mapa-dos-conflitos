    </div><!-- Main container -->
<?php if(!isset($_GET['spajax'])): ?>
    <?php wp_footer(); ?>
    <script type="text/javascript">
        const home_url = '<?php echo home_url(); ?>';
        const template_url = '<?php tu(); ?>/';
        const data_url = '<?php echo home_url(); ?>/assets/data/';
    </script>
    <audio preload="auto" style="display: none;" id="sd_click"><source src="<?php tu(); ?>/assets/sounds/click.mp3" type="audio/mpeg"/></audio>
    <audio preload="auto" style="display: none;" id="sd_close"><source src="<?php tu(); ?>/assets/sounds/close.mp3" type="audio/mpeg"/></audio>
    <audio preload="auto" style="display: none;" id="sd_drop"><source src="<?php tu(); ?>/assets/sounds/drop.mp3" type="audio/mpeg"/></audio>
    <audio preload="auto" style="display: none;" id="sd_hover"><source src="<?php tu(); ?>/assets/sounds/hover.mp3" type="audio/mpeg"/></audio>
    <audio preload="auto" style="display: none;" id="sd_hover2"><source src="<?php tu(); ?>/assets/sounds/hover2.mp3" type="audio/mpeg"/></audio>
    <script src="<?php tu(); ?>/build/script.js"></script>
</body>    
</html>
<?php endif; ?>