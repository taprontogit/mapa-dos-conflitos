<?php
get_header(); 
?>

<main class="institucional sobre">
    <div class="container-fluid wrap">

        <h2>Methodology</h2>
        
        <p>How the scores for each municipality are calculated.</p>
        
        <p class="mb05"><strong>Scores</strong> are calculated in three ways:</p>
        
        <ol>
            <li><strong>Using raw numbers</strong>, in the categories water, conflict, and inequality;</li>
            <li><strong>Proportionally by population of municipality</strong>, in the categories agrotoxins and violence;</li>
            <li><strong>Proportionally by land area of municipality</strong>, in the categories deforestation, wildfires, and mining;</li>
        </ol>

        <div>
            <h3>Conflicts</h3>
            <p>The number of conflicts by municipality was obtained using research by the CPT. In the CPT’s database, a single conflict may affect more than one municipality. As such, the municipalities were taken into account for each instance of conflict. The raw number of conflicts was used to classify scores, without adjustment for land area or population.</p>

            <h3>IDH-M</h3>
            <p>The most recent data for the Municipal Human Development Index is from 2010. The United Nations Development Programme (UNDP) ranking was adapted for creation of the bands:</p>
            <table>
                <tbody>
                    <tr>
                        <td>Municipal Human Development Index (IDH-M)</td>
                        <td>Band</td>
                    </tr>
                    <tr>
                        <td>0 (no data)</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>0,001-0,499</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>0,500-0,599</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>0,600-0,699</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>0,700-0,799</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>0,800-1,000</td>
                        <td>1</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div>
            <h3>Mining</h3>
            <p>Information on mining processes was obtained using open data from the National Mining Agency (ANM). Figures were calculated by municipality and year, and adjusted using the following formula:</p>
            <div class="equacao">
                <p>Mining <sub>score</sub> = </p> 
                <p class="fracao"><sup>processes</sup><sub>area</sub></p> 
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Wildfires</h3>
            <p>The number of fires for each municipality was calculated using data from INPE&rsquo;s <a href="https://queimadas.dgi.inpe.br/queimadas/bdqueimadas">BDQueimadas</a> platform. We looked at the fires in the Legal Amazon between 2011 and 2020 which were recorded by INPE&rsquo;s &lsquo;reference satellite&rsquo; (AQUA-MT).</p>
            <p>Figures were calculated by municipality and year, and adjusted using the following formula:</p>
            <div class="equacao">
                <p>Wildfires <sub>score</sub> = </p> 
                <p class="fracao"><sup>fires</sup><sub>area</sub></p> 
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Deforestation</h3>
            <p>The data for deforestation was obtained using INPE&rsquo;s <a href="http://www.dpi.inpe.br/prodesdigital/prodesmunicipal.php">Prodes</a> system. The data used was for increases in deforestation by municipality and by year, in the period 2011 to 2020. The increase in deforestation was adjusted for the area of the municipality, using the following formula:</p> 
            <div class="equacao">
                <p>Deforestation <sub>score</sub> = </p> 
                <p class="fracao"><sup>increase</sup><sub>area</sub></p>
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Agrotoxins</h3>
            <p>The data consists of reports of exogenous intoxication registered on Sinan-Net and were obtained from the platform <a href="https://datasus.saude.gov.br/acesso-a-informacao/doencas-e-agravos-de-notificacao-de-2007-em-diante-sinan/">Tabnet &ndash; DataSUS</a>, from the Ministry of Health. The criteria applied for the sample were:</p>
            <ul>
                <li>Reports by municipality of residence and year of first symptoms.</li>
                <li>Toxic agent: a toxin used in agriculture.</li>
                <li>Circumstance: habitual, accidental, or environmental use.</li>
                <li>Final classification: confirmed case of intoxication.</li>
                <li>Period: 2011-2020.</li>
            </ul>
            <p>The reports were adjusted for the size of the municipal population, using the following formula:</p>
            <div class="equacao">
                <p>Agrotoxins <sub>score</sub> = </p> 
                <p class="fracao"><sup>reports</sup><sub>population</sub></p> 
                <p> x 100000</p> 
            </div>
        </div>

        <div>
            <h3>Violence</h3>
            <p>The data was obtained from the platform <a href="https://datasus.saude.gov.br/acesso-a-informacao/doencas-e-agravos-de-notificacao-de-2007-em-diante-sinan/">Tabnet &ndash; DataSUS</a>, from the Ministry of Health. The criteria applied for the sample were:</p>
            <ul>
            <li>Hospital morbidity in the Unified Health System (SUS) due to external causes.</li>
            <li>Hospitalizations by municipality of residence and year.</li>
            <li>Wider group of causes: interpersonal violence classified under ICD 10 codes X85-Y09.</li>
            <li>Period: 2011-2020.</li>
            </ul>
            <p>The reports were adjusted according for the size of the municipal population, using the following formula:</p>
            <div class="equacao">
                <p>Violence <sub>score</sub> = </p> 
                <p class="fracao"><sup>reports</sup><sub>population</sub></p> 
                <p> x 100000</p> 
            </div>
        </div>

        <div>
            <h3>Water</h3>
            <p>Data on water use permits were downloaded from the National Water Agency's open data, aggregated by municipality and year. Only the grants with the purpose of right of use were filtered.</p>
        </div>

        <div>
            <h3>Bands</h3>
            <p>Each category is divided into bands, ranging from 1 (the lowest) to 5 (the highest). The value 0 corresponds to municipalities for which there is no information or with no instances registered for the chosen category. Apart from the ‘Inequality’ category described above, the bands were divided according to the following criteria:</p>
            <table>
                <tbody>
                    <tr>
                        <td>Band</td>
                        <td>Cut-off point</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Scores above Q3 + 3*IIQ (outliers, identified in this <a href="https://github.com/biamuniz/mapadosconflitos/blob/main/code/id_outliers.Rmd">script</a>)</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Above 3*(Q3/4) up to Q3</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Above 2*(Q3/4) up to 3*(Q3/4)</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Above Q3/4 up to 2*(Q3/4)</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Above 0 up to Q3/4</td>
                    </tr>
                    <tr>
                        <td>0</td>
                        <td>No information/no instance registered</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div>
            <h3>Glossary</h3>
            <ul>
            <li>ID: unique, five-digit identification of record/line.</li>
            <li>LENTE: category to be highlighted.</li>
            <li>CD_MUN: six-digit municipality identification from the Brazilian Institute of Geography and Statistics (IBGE).</li>
            <li>UF: state</li>
            <li>NM_MUN: name of municipality</li>
            <li>ANO: year of instance</li>
            <li>VALOR: number of reports, instances, etc.</li>
            <li>VALOR_PADRONIZADO: the column &ldquo;VALOR&rdquo; after adjustment for area or population, used to calculate the bands.</li>
            <li>FAIXAS: the bands from 0 to 5, calculated according to &ldquo;VALOR_PADRONIZADO&rdquo;</li>
            <li>TEXTO: text highlighting some feature of the municipality according to the category.</li>
            </ul>
        </div>
        
        <div class="mt3">

            <!-- Begin Mailchimp Signup Form -->

            <div id="mc_embed_signup">
                <form action="https://apublica.us8.list-manage.com/subscribe/post?u=47bdda836f3b890e13c9f416d&amp;id=b7667a23e7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                    <h3>Receive the Map of Conflict database</h3>
                <div class="mc-field-group">
                    <label for="mce-EMAIL">E-mail  <span class="asterisk">*</span>
                </label>
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                </div>
                <div class="mc-field-group">
                    <label for="mce-FNAME">Name  <span class="asterisk">*</span>
                </label>
                    <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
                </div>
                <div class="mc-field-group">
                    <label for="mce-LNAME">Surname </label>
                    <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
                </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_47bdda836f3b890e13c9f416d_b7667a23e7" tabindex="-1" value=""></div>
                    <div class="clear"><input class="btn" type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"></div>
                    </div>
                </form>
                </div>
                <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone'; /**
                * Translated default messages for the $ validation plugin.
                * Locale: PT_PT
                */
                $.extend($.validator.messages, {
                    required: "Required field",
                    remote: "Please correct this field.",
                    email: "Please enter a valid email address",
                    url: "Por favor, introduza um URL v&aacute;lido.",
                    date: "Por favor, introduza uma data v&aacute;lida.",
                    dateISO: "Por favor, introduza uma data v&aacute;lida (ISO).",
                    number: "Por favor, introduza um n&uacute;mero v&aacute;lido.",
                    digits: "Por favor, introduza apenas d&iacute;gitos.",
                    creditcard: "Por favor, introduza um n&uacute;mero de cart&atilde;o de cr&eacute;dito v&aacute;lido.",
                    equalTo: "Por favor, introduza de novo o mesmo valor.",
                    accept: "Por favor, introduza um ficheiro com uma extens&atilde;o v&aacute;lida.",
                    maxlength: $.validator.format("Por favor, n&atilde;o introduza mais do que {0} caracteres."),
                    minlength: $.validator.format("Por favor, introduza pelo menos {0} caracteres."),
                    rangelength: $.validator.format("Por favor, introduza entre {0} e {1} caracteres."),
                    range: $.validator.format("Por favor, introduza um valor entre {0} e {1}."),
                    max: $.validator.format("Por favor, introduza um valor menor ou igual a {0}."),
                    min: $.validator.format("Por favor, introduza um valor maior ou igual a {0}.")
                });}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                <!--End mc_embed_signup-->

                </div>

            </div>

        </div>
</main>

<?php get_footer(); ?>

            