<?php
get_header(); 
?>

<main class="institucional">
    <div class="container-fluid wrap">
        <h2>Reportagens</h2>
        <div class="row acenter">
            <div class="col-md-6 col-xs-12 mb2">
                <a href="https://apublica.org/2022/04/assassinatos-no-campo-em-2021-batem-recorde-dos-ultimos-quatro-anos/" target="_blank" class="reportagem">
                    <div class="zoomimg"><img src="<?php tu(); ?>/assets/images/r1.jpg"></div>
                    <h3>Assassinatos no campo em 2021 batem recorde dos últimos quatro anos</h3>
                    <p>Sob Bolsonaro, a média de ocorrências de conflitos já é a maior da história. No ano passado, 35 pessoas foram assassinadas no campo, 29 somente na Amazônia.</p>
                </a>
                <a href="https://apublica.org/2022/04/assassinatos-no-campo-em-2021-batem-recorde-dos-ultimos-quatro-anos/" target="_blank" class="btn">ler reportagem</a>
            </div>
            <div class="col-md-6 col-xs-12 mb2">
                <a href="https://apublica.org/2022/04/mais-de-cem-mil-familias-foram-afetadas-em-uma-decada-de-conflitos-no-campo-na-amazonia/" target="_blank" class="reportagem">
                    <div class="zoomimg"><img src="<?php tu(); ?>/assets/images/r2.jpg"></div>
                    <h3 class="pb1">Mais de cem famílias foram afetadas em uma década de conflitos no campo da amazônia</h3>
                    <p>Mapa dos conflitos, ferramenta exclusiva lançada pela Agência Pública e CPT, mapeia conflitos e dados socioambientais de todos os municípios da Amazônia Legal</p>
                </a>
                <a href="https://apublica.org/2022/04/mais-de-cem-mil-familias-foram-afetadas-em-uma-decada-de-conflitos-no-campo-na-amazonia/" target="_blank" class="btn">ler reportagem</a>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>