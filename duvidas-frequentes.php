<?php
get_header(); 
?>

<main class="institucional sobre">
    <div class="container-fluid wrap">

        <h2>Dúvidas frequentes</h2>
        
        <h3>O que é o Mapa dos Conflitos?</h3>
        <p>O Mapa é um levantamento de dados de uma década de conflitos registrados pela Comissão Pastoral da Terra (CPT). A Pública cruzou esses dados com indicadores sociais e ambientais que mostram o cenário dos municípios da Amazônia Legal.</p>

        <h3>O que são as lentes?</h3>
        <p>Cada lente é um conjunto de dados da década que mostram alguma situação socioambiental nos municípios da Amazônia Legal: quantos pedidos de mineração foram protocolados, qual foi o aumento do desmatamento, quantos focos de incêndio foram registrados…</p>

        <h3>De onde vêm os dados?</h3>
        <p>Os dados de conflitos vêm dos registros realizados pela CPT ao longo destes dez anos. Já os dados socioambientais são todos de bases públicas, boa parte delas geridas pelo governo federal, como DataSUS e o BDQUeimadas, do Instituto Nacional de Pesquisas Espaciais. Na seção de <a href="https://apublica.org/mapadosconflitos/metodologia">metodologia</a> explicamos a origem dos dados.</p>

        <h3>Como interpretar os dados?</h3>
        <p>Os dados mostram diferentes aspectos da situação socioambiental no município. Eles podem indicar situações correlacionadas, como conflitos nos quais famílias foram atingidas por agrotóxicos, ou não. Nesta <a href="https://apublica.org/2022/04/mais-de-cem-mil-familias-foram-afetadas-em-uma-decada-de-conflitos-no-campo-na-amazonia/" target="_blank">reportagem</a> explicamos quais indicadores costumam ter mais relação entre si, como queimadas e desmatamento. Encorajamos pesquisadores, jornalistas e cidadãos a usarem o Mapa dos Conflitos como ponto de partida para entender melhor a situação de um município.</p>

        <h3>Os dados do Mapa dos Conflitos podem ser utilizados?</h3>
        <p>Todo o conteúdo produzido pela Pública é liberado para republicação gratuita, contando que seja republicado na íntegra e com o crédito. Dados individuais do mapa, como a quantidade de conflitos em um município, por exemplo, podem ser utilizados em outras publicações com a devida referência. Cálculos e conclusões realizados por terceiros a partir dos dados do Mapa são de responsabilidade de quem os fizer.</p>

        <h3>Como dar o crédito?</h3>
        <p>O Mapa dos Conflitos é uma ferramenta da <a href="https://apublica.org/" target="_blank">Agência Pública</a> em parceria com a <a href="https://www.cptnacional.org.br/" target="_blank">Comissão Pastoral da Terra</a> — Acesse: <a href="https://apublica.org/mapadosconflitos/" target="_blank">apublica.org/mapadosconflitos</a></p>

        <h3>Posso republicar o material?</h3>
        <p>Somos uma agência sem fins lucrativos que distribui reportagens investigativas gratuitamente, sob a licença Creative Commons CC BY-ND. Seu site também pode republicar os nossos conteúdos. Basta seguir as nossas normas. Saiba mais sobre como <a href="https://apublica.org/republique/" target="_blank">republicar</a> nosso material.</p>

    </div>
</main>

<?php get_footer(); ?>