<?php
get_header(); 
?>

<main class="institucional expediente">
    <div class="container-fluid wrap">
        <h2>Expediente</h2>
        <div class="row between-xs">
            <div class="col-md-3 col-xs-12">
                <p class="label">Coordenação geral e concepção</p>
                <p>Thiago Domenici</p>
                <p class="label">Coordenação de dados</p>
                <p>Bruno Fonseca</p>
                <p class="label">Analista de dados</p>
                <p>Bianca Muniz</p>

            </div>
            <div class="col-md-3 col-xs-12">
                <p class="label">Reportagens</p>
                <p>Bianca Muniz, Bruno Fonseca, Rafael Oliveira</p>
                <p class="label">Animação</p>
                <p>Itamar Vieira Junior, Thiago Domenici, Anna Beatriz Anjos, Caetano Patta, Fernando Guimarães, Rafael Oliveira, Clarissa Levy, Victor Brasileiro</p>
            </div>
            <div class="col-md-3 col-xs-12">
                <p class="label">Comissão Pastoral da Terra (CPT)</p>
                <p>Flávio Marcos Gonçalves de Araújo — Centro de Documentação Dom Tomás Balduino</p>
                <p class="label">Design e Programação</p>
                <p><a href="https://cafe.art.br/" target="_blank">Café.art.br</a></p>
            </div>
        </div>
    </div>
</main>


<?php get_footer(); ?>