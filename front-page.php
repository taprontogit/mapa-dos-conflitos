<?php
$current_page = 'home';
get_header(); 
?>

<main id="intro">
    <div class="intro-section intro-1">
        <div class="center">
            <div class="logoglitch"><img src="<?php tu(); ?>/assets/images/main-logo.svg" alt="Mapa dos Conflitos"><img class="lglitch" src="<?php tu(); ?>/assets/images/main-logo-c.svg" alt="Mapa dos Conflitos"></div>
            <p>Uma década de violência e injustiça fundiária na Amazônia Legal</p>
        </div>
        <div class="apoiadores">
            <img src="<?php tu(); ?>/assets/images/publica.png">
            <img src="<?php tu(); ?>/assets/images/cpt.png">
        </div>
    </div>
    <div class="intro-section intro-2">
        <div class="center">
            <div>
                <h2>Amazônia Legal</h2>
                <p>O Mapa dos Conflitos foca sua análise na Amazônia Legal, região com mais de 5 milhões de km<sup>2</sup>, representando dois terços do país. A área inclui os estados do Amazonas, Roraima, Rondônia, Pará, Amapá, Acre, Tocantins, Mato Grosso e parte do Maranhão. É na Amazônia Legal onde se concentram 55% dos conflitos no campo entre 2011 e 2020.</p>
            </div>
        </div>
    </div>
    <div class="intro-section intro-big">
        <div class="center">
            <div>
                <div class="frases">
                    <h2>A última década na Amazônia</h2>
                    <span class="data">(2011-2020)</span>
                    <h4><strong>+ de 300</strong> assassinatos</h4>
                    <h4><strong>+ de 2 mil</strong> vítimas</h4>
                    <h4><strong>+ de 100 mil</strong> famílias afetadas</h4>
                    <h4><strong>+ de 7 mil</strong> ocorrências de conflito</h4>
                    <h4><strong>9</strong> estados e <strong>583</strong> municípios afetados</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-section conflitos-section">
        <div class="center">
            <div style="overflow-x: hidden;">
                <?php get_template_part('blocks/_temas-intro'); ?>
                <div class="fixpos">
                    <div class="infos-geral">
                        <h2>Mapa dos conflitos</h2>
                        <p>Navegue pelas lentes temáticas e descubra como se relacionam os conflitos no campo com desmatamento, queimadas, violência, desigualdade, agrotóxicos, água e mineração. Para melhor navegação, veja o que representa cada lente.</p>
                    </div>
                    <div class="infos-tema" data-indicador="1">
                        <h2>Conflitos</h2>
                        <p>As ocorrências de conflitos no campo são informações colhidas pela CPT anualmente e sistematizadas pelo Centro de Documentação Dom Tomás Balduino – CEDOC.</p>
                    </div>
                    <div class="infos-tema" data-indicador="6">
                        <h2>Queimadas</h2>
                        <p>Os focos de queimadas são medidos pelo INPE. Esta lente mostra a quantidade de focos de queimadas em relação à área do município.</p>
                    </div>
                    <div class="infos-tema" data-indicador="4">
                        <h2>Desmatamento</h2>
                        <p>Quanto maior a classificação, maior o avanço do desmatamento no município medido pelo Instituto Nacional de Pesquisas Espaciais (Inpe). A lente considera o tamanho do município para o cálculo.</p>
                    </div>
                    <div class="infos-tema" data-indicador="2">
                        <h2>Agrotóxicos</h2>
                        <p>A lente traz a quantidade de registros de intoxicações causadas por agrotóxicos para uso agrícola. As intoxicações são causadas por acidentes no manuseio de agrotóxicos, no trabalho ou na pulverização. </p>
                    </div>
                    <div class="infos-tema" data-indicador="8">
                        <h2>Desigualdade</h2>
                        <p>Aqui comparamos dados que refletem a qualidade de vida da população a partir do Índice de Desenvolvimento Humano Municipal (IDH-M): expectativa de vida, anos de escolaridade e renda <i>per capita</i>.</p>
                    </div>
                    <div class="infos-tema" data-indicador="3">
                        <h2>Água</h2>
                        <p>Para utilizar água de rios, lagos e outras fontes para lavouras e indústrias, é preciso que os governos concedam outorgas. Esta lente mostra a quantidade de outorgas para o uso de água nos municípios.</p>
                    </div>
                    <div class="infos-tema" data-indicador="5">
                        <h2>Mineração</h2>
                        <p>Para minerar de forma legal, os pedidos são feitos na Agência Nacional de Mineração (ANM). Esta lente mostra a quantidade desses pedidos por município, levando em conta o seu tamanho.</p>
                    </div>
                    <div class="infos-tema" data-indicador="7">
                        <h2>Violência</h2>
                        <p>A lente mostra o número de internações em hospitais após as vítimas terem sofrido agressões de diversos tipos. O cálculo leva em consideração o tamanho da população do município.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-section fim">
    </div>

    <div class="scrollTo"><span></span></div>
    <a href="<?php echo home_url('mapa'); ?>" class="btn">pular introdução</a>
    <div class="vinheta"></div>
</main>

<?php get_footer(); ?>