// Vars
var wst=0,
    wh=0,
    ww=0,
    topBar = document.getElementById('topBar'),
    mainContent = document.getElementById('mainContent'),
    loader = document.getElementById('loader'),
    _scrollEvents = [];

// Helper Functions
function loading(a){
    document.body.classList.toggle('loader-on',a);
    loader.classList.toggle('out',!a);
}
const addScroll = (func) => {
    _scrollEvents.push({ func: func });
}
const removeAllScrollEvents = () => {
    _scrollEvents = [];
}
function addParam(url,param){
    url += (url.split('?')[1] ? '&':'?') + param;
    return url;
}
function scrollWatch(event){
    wst = window.scrollY || window.pageYOffset || document.documentElement.scrollTop;
    _scrollEvents.forEach(function(e) { e.func(); });
}
function parseCSV(str, delimiter = ";") {
  let le = str.indexOf('\r') !== -1;
  let lineBreak = le ? '\r\n' : '\n';
  let headers = str.slice(0, str.indexOf(lineBreak)).split(delimiter);
  let rows = str.slice(str.indexOf(lineBreak) + (le?2:1)).split(lineBreak);
  let arr = rows.map(function (row) {
    let values = row.split(delimiter);
    let el = headers.reduce(function (object, header, index) {
      object[header] = values[index];
      return object;
    }, {});
    return el;
  });
  return arr;
}
function getFile(link,cb){
    let http = new XMLHttpRequest();
    http.open('GET',link, true);
    http.onreadystatechange = function() {
        if(http.readyState == 4 && http.status == 200) {
            cb(http);
        }
    }
    http.send();
}
function trigger(el,ev){
    if(document.createEvent){
        el.dispatchEvent(new Event(ev));
    }
}
const cleanName = function(s){
    if(!s)return s;
    s = s.toLowerCase();
    s = s.replace(/[ÁÀÂÃ]+/gi, "a");
    s = s.replace(/[ÉÈÊ]+/gi, "e");
    s = s.replace(/[ÍÌÎ]+/gi, "i");
    s = s.replace(/[ÓÒÔÕ]+/gi, "o");
    s = s.replace(/[ÚÙÛ]+/gi, "u");
    s = s.replace(/[Ç]+/gi, "c");
    s = s.replace(/[Ñ]+/gi, "n");
    s = s.replace("'", "_");
    s = s.replace(/[^A-Za-z\s\-\_]+/gi, "");
    s = s.replace(/\s+/gi, "_");
    s = s.replace(/-+/gi, "-");
    return s;
}
function getOffsetTop(element){
  let offsetTop = 0;
  while(element) {
    offsetTop += element.offsetTop;
    element = element.offsetParent;
  }
  return offsetTop;
}
function play(c){
    if(a = document.getElementById('sd_'+c)) a.play();
}
function toFixed(num, fixed) {
    return new Intl.NumberFormat('pt-BR', { minimumFractionDigits: fixed, maximumFractionDigits: fixed }).format(num);
}
// General
var generalApp = {

    init: function(){
        // Vars
        wh = window.innerHeight;
        ww = window.innerWidth;
        topBarH = topBar.offsetHeight;
        
        removeAllScrollEvents();
        addScroll(()=>{topBar.classList.toggle("scroll-on",window.pageYOffset>1)});

        const selectOptions = document.querySelectorAll('.select-option');
        selectOptions.forEach(option => {
            option.addEventListener('click', event => {
                const selectForm = option.closest('.selectbox');
                selectForm.querySelector('.label-select').innerHTML = option.innerHTML;
                var v = selectForm.querySelector('.select-value');
                v.value = option.getAttribute('data-value');
                trigger(v,'change');
                selectForm.querySelector('.check-drop').checked = false;
                
            })
        });

        document.querySelectorAll('a[href]').forEach(function(a){
            if(a.href && a.href.indexOf(window.location.host) && a.target != "_blank" && !a.attributes.role && a.target != "_self" && !a.classList.contains('anchor-target') ){
                a.onclick = function(event){
                    event.preventDefault(); 
                    generalApp.changePage(this.href); 
                    play('click');
                    return false; 
                };
            }else if(a.classList.contains('anchor-target')){
                a.onclick = function(event){
                    event.preventDefault(); 
                    var d = document.getElementById(this.href.split("#")[1]);
                    var y = getOffsetTop(d);
                    play('click');
                    if(d) window.scrollTo(0,y);
                    return false; 
                };
            }
            a.onmouseenter=()=>{play('hover')};
            if(a.href==document.location.href) a.classList.add('active');
        });
        loading(0);
    },
    changePage: function(url){
        loading(1);
        removeAllScrollEvents();

        if(!generalApp.timerPage){
            generalApp.timerPage = setTimeout(function(){
                window.history.pushState("", "", url);
                let http = new XMLHttpRequest();
                http.open('GET', addParam(url,"spajax=true"), true);
                http.onreadystatechange = function() {
                    if(http.readyState == 4 && http.status == 200) {
                        mainContent.innerHTML = http.responseText;
                        if(mainContent.outerHTML) {
                            mainContent.outerHTML=http.responseText;
                        }else{
                            var tmpObj=document.createElement("div");
                            tmpObj.innerHTML='<!--THIS DATA SHOULD BE REPLACED-->';
                            ObjParent=mainContent.parentNode;
                            ObjParent.replaceChild(tmpObj,mainContent);
                            ObjParent.innerHTML=ObjParent.innerHTML.replace('<div><!--THIS DATA SHOULD BE REPLACED--></div>',http.responseText);
                        }
                        setTimeout(function(){
                            mainContent = document.getElementById("mainContent");
                            topBar = document.getElementById('topBar');
                            topBarH = topBar.offsetHeight;
                            var images = mainContent.getElementsByTagName("img");
                            var loaded = images.length;
                            for (var i = 0; i < images.length; i++) {
                                if (images[i].complete){
                                    loaded--;
                                }else{
                                    images[i].addEventListener("load", function() {
                                        loaded--;
                                        if (loaded == 0){
                                            start();
                                        }
                                    });
                                }
                            }
                            start();
                        },900);
                    }else if(http.readyState == 4) {
                        start();
                    }
                    window.scrollTo(0,0);
                    clearTimeout(generalApp.timerPage);
                    generalApp.timerPage = false;
                }
                http.send();
            },700);
        }
    },
}

// Home
var homeApp = {
    size: 0,
    glitch: false,
    timer: false,
    init: function(){
        var intros = document.querySelectorAll('.intro-section');
        var introsbig = document.querySelectorAll('.intro-big');
        var introsconflitos = document.querySelectorAll('.conflitos-section');
        this.size = (mainContent.offsetHeight-wh)/(intros.length+(introsbig.length*3)+(introsconflitos.length*10)-1);
        addScroll(this.homeAnimation);
        loading(0);
    },
    homeAnimation: function(){
        var c = wst/homeApp.size;
        if(c>=2&&c<5){
            c = Math.floor(c)+'.'+Math.floor((c-2)/3*7);
        }else{
            c = Math.floor(c); 
        }
        
        mainContent.dataset.intro = c;
        if(c>=16) document.location = home_url+'mapa';
    }
}

// Mapa
$municipios = false;
$lentes = false;
$baseMap = false;
$dados = false;
var mapApp = {
    map: false,
    tooltip: false,
    mapcard: false,
    cardcontent: false,
    basemap: false,
    pane: false,
    indicator: 1,
    year: 2020,
    lente: false,
    searchvalue: false,
    zoomLayer: false,
    zoommed: false,
    markers: [],
    init: function(){
        this.load();
    },
    load: function(){
        loading(1);
        if($municipios===false){
            $municipios = 0;
            getFile(data_url+'municipios.csv', (http)=>{
                $municipios = parseCSV(http.responseText);
                mapApp.mountMap();
            });
        }
        if($lentes===false){
            $lentes = 0;
            getFile(data_url+'lentes.csv', (http)=>{
                $lentes = parseCSV(http.responseText);
                mapApp.mountMap();
            });
        }
        if($baseMap===false){
            $baseMap = 0;
            getFile(template_url+'assets/map/map.min.json', (http)=>{
                $baseMap = JSON.parse(http.responseText);
                mapApp.mountMap();
            });
        }
        if($dados===false){
            $dados = 0;
            getFile(data_url+'dados.csv', (http)=>{
                $dados = parseCSV(http.responseText);
                mapApp.mountMap();
            });
        }
        if(
            $municipios&&
            $lentes&&
            $baseMap&&
            $dados
        ){
            mapApp.mountMap();
        }
    },
    mountMap: function(){
        if(!$municipios||!$lentes||!$baseMap){this.load(); return false;}
        var map = document.getElementById("map");
        this.tooltip = document.getElementById("tooltipmap");
        this.mapcard = document.getElementById('mapcard');
        this.cardcontent = document.getElementById('cardcontent');

        if(map && !map.classList.contains('leaflet-container')){
            this.map = L.map('map',{
                zoomControl:false, minZoom:4, maxZoom:8, zoomSnap: 0.5,
                maxBoundsViscosity: 0.7
            }).setView([-4.554, -58.281], 5)
            .setMaxBounds([[15,-97],[-27,-27]]);
            
            L.tileLayer(template_url+'assets/map/tiles/{z}/{x}/{y}.png', {
              minZoom: 1,
              maxZoom: 8
            }).addTo(this.map);

            this.map.on('zoom', function(e){
                L.DomEvent.stopPropagation(e);
            })

            this.pane = document.querySelector('.leaflet-map-pane');
        }
        mapApp.filter();
        loading(0);
    },
    filter: function(){
        if(!$municipios||!$dados){this.load(); return false;}
        mapApp.mapcard.classList.remove("active");
        //this.pane.classList.remove('perspective');
        var mi = {};
        $dados.forEach(a=>{
            if(!mi[a.CD_MUN]) mi[a.CD_MUN] = {};
            if(!mi[a.CD_MUN][a.CD_LENTE]) mi[a.CD_MUN][a.CD_LENTE] = false;
            if(a.ANO==mapApp.year) mi[a.CD_MUN][a.CD_LENTE] = a;
        });
        for (var i = 0; i < $baseMap.features.length; i++) {
            var a = $baseMap.features[i];
            var c = $municipios.filter((b)=>b.CD_MUN==a.properties.id);
            a.info = c ? c[0] : false;
            a.dados = mi[a.properties.id] ? mi[a.properties.id] : false;
        }

        var o = '';
        for (var i = 0; i < $municipios.length; i++) {
            var a = $municipios[i];
            /*var c = $baseMap.features.filter((b)=>a.CD_MUN==b.properties.id);
            if(!c.length){console.log(a);}*/
            a.slug = cleanName(a.NM_MUN+'-'+a.UF);
        }
        document.getElementById('buscamunicipio').innerHTML = o;

        if(mapApp.markers){
            mapApp.markers.forEach(x=>{
                x.remove();
            });
        }
        mapApp.markers = [];
        mainContent.dataset.tipo = mapApp.indicator;
        var l = $lentes.filter(a=>a.CD_LENTE==mapApp.indicator);
        this.lente = (l.length)? l[0]:false;
        this.legenda();
        this.render();
    },
    search:function(t){
        var v = t.value;
        var o = '';
        if(v.length){
            mapApp.searchvalue = cleanName(v);
            for (var i = 0; i < $municipios.length; i++) {
                var a = $municipios[i];
                if(a.slug && a.slug.indexOf(mapApp.searchvalue) != -1)o+=`<option value="${a.NM_MUN+'-'+a.UF}">`;
                if(a.slug == mapApp.searchvalue){
                    mapApp.zoommed = [{info:a}];
                    mapApp.filter();
                    t.blur();
                    t.value="";
                }
            }
        }
        document.getElementById('buscamunicipio').innerHTML = o;
    },
    render: function(){
        if(!$dados){this.load(); return false;}
        var m = this.map;
        if(mapApp.currentMap) m.removeLayer(mapApp.currentMap);
        var zoomElements = [];

        mapApp.currentMap = L.geoJson($baseMap, {
            clickable: true,
            style: function(a){
                return {
                    weight: 1,
                    color: "none",
                    fillColor: "none",
                    fillOpacity: .98,
                    className: 'shape-not-assessed',
                };
            },
            onEachFeature: function(a,t){
                var c = a.info;
                var d = a.dados;
                if(c){
                    t.setStyle({
                        'className': c?'indicator-'+mapApp.indicator+' l'+(d[mapApp.indicator]?d[mapApp.indicator].FAIXA:'0'):'shape-not-assessed'
                    });

                    if(mapApp.zoommed&&mapApp.zoommed[0].info.CD_MUN==c.CD_MUN){
                        mapApp.zoommed = [a,t];
                    }

                    if(d[mapApp.indicator] && d[mapApp.indicator].TEXTO){
                        var center = [(t.getBounds()).getCenter()];
                        var p = L.marker(center[0], {icon:L.icon({
                            iconUrl: template_url+'assets/images/pinrecordista.svg',
                            iconSize: [40, 40]
                        }),zIndexOffset:9999})
                        .on("mouseover",function(e){
                            mapApp.mouseoverShape(a,t,e);
                            t._path.classList.add('hover');
                            play('hover2');
                        }).on("mousemove",function(e){
                            mapApp.positionTooltip(e);
                        }).on("mouseout",function(e){
                            mapApp.tooltip.classList.remove("active");
                            t._path.classList.remove('hover');
                        }).on("click",function(e){
                            play('click');
                            if(t!=mapApp.zoommed[1]){
                                mapApp.clickShape(a,t,e);
                            }else{
                                mapApp.zoom();
                            }
                        }).addTo(m);
                        mapApp.markers.push(p);
                    }
                    zoomElements.push(t.feature);

                    // Mouse interactions
                    t.on("mouseover",function(e){
                        mapApp.mouseoverShape(a,t,e);
                        t._path.classList.add('hover');
                        play('hover2');
                    }).on("mousemove",function(e){
                        mapApp.positionTooltip(e);
                    }).on("mouseout",function(e){
                        mapApp.tooltip.classList.remove("active");
                        t._path.classList.remove('hover');
                    }).on("click",function(e){
                        play('click');
                        if(t!=mapApp.zoommed[1]){
                            mapApp.clickShape(a,t,e);
                        }else{
                            mapApp.zoom();
                        }
                    });
                }
            }
        }).addTo(m);

        this.zoomLayer = zoomElements.length ? L.geoJson(zoomElements) : mapApp.currentMap;
        if(mapApp.zoommed){
            mapApp.clickShape(mapApp.zoommed[0],mapApp.zoommed[1]);
        }else if(this.zoomLayer){
            m.fitBounds(this.zoomLayer.getBounds().pad(0.2));
            m.setMaxBounds(this.zoomLayer.getBounds().pad(0.2));
        }
    },
    legenda: function(){
        var o=`
            <div class="escala-0">0</div>
            <div class="escala-1">${mapApp.lente.ESCALA1}</div>
            <div class="escala-2">${mapApp.lente.ESCALA2}</div>
            <div class="escala-3">${mapApp.lente.ESCALA3}</div>
            <div class="escala-4">${mapApp.lente.ESCALA4}</div>
            <div class="escala-5">${mapApp.lente.ESCALA5}</div>
        `;
        document.getElementById('escalamapa').innerHTML = o;
        document.getElementById('escalalabel').innerHTML = mapApp.lente.LABEL;

    },
    tooltipCard: function(a){
        var c = a.info;
        var d = a.dados;
        var o = `<div>
            <p class="label">Local</p>
            <h3>${c.NM_MUN}, ${c.UF}</h3>
        </div>`;
        if(d[mapApp.indicator])o+=`<div>
            <p class="label">${mapApp.lente.LABEL}</p>
            <h2>${ toFixed(d[mapApp.indicator].VALOR_PADRONIZADO,mapApp.lente.DECIMAIS)}</h2>
        </div>`;
        return o;
    },
    mouseoverShape: function(a,t,e){
        // Tooltip
        if(mapApp.indicator==0) return false;
        //if(mapApp.zoommed) return false;
        var o = mapApp.tooltipCard(a);
        mapApp.tooltip.innerHTML = o;
        mapApp.tooltip.classList.add("active");
        mapApp.positionTooltip(e);
    },
    positionTooltip: function(e){
        var tooltip = this.tooltip;
        var ml = tooltip.offsetWidth/2;
        var mt = tooltip.offsetHeight;
        var x = e.originalEvent.clientX;
        var y = e.originalEvent.clientY;
        var top = Math.min(wh-mt-50,Math.max(topBarH,y-mt-50));
        var left = x+ml;

        tooltip.classList.remove('right','left');
        if(x > ww*0.5){
            left = x-ml;
            tooltip.classList.add('right');
        }else{
            tooltip.classList.add('left');
        }
        tooltip.style.left = left+"px";
        tooltip.style.top = top+"px";
        tooltip.style.marginLeft = -ml+"px";
    },
    clickShape: function(a,t,e){
        if(!mapApp.lente) return false;
        var c = a.info;
        var d = a.dados;
        // Tooltip
        var o = `
            <div>
                <h3 class="local">${c.NM_MUN}, ${c.UF}</h3>
                <h3 class="lente">${mapApp.indicator>1 ? $lentes[0].LENTE+' x '+mapApp.lente.LENTE : mapApp.lente.LENTE}</h3>
            </div>`;
            o+=`<div>
                <p class="label">${$lentes[0].LENTE} <b>${toFixed(d["1"].VALOR_PADRONIZADO,$lentes[0].DECIMAIS)}</b></p>
                <div class="escala-conflitos">
                    <div class="escala-0">faixa 0</div>
                    <div class="escala-1 ${d["1"].FAIXA=="1"?'active':''}">faixa 1</div>
                    <div class="escala-2 ${d["1"].FAIXA=="2"?'active':''}">faixa 2</div>
                    <div class="escala-3 ${d["1"].FAIXA=="3"?'active':''}">faixa 3</div>
                    <div class="escala-4 ${d["1"].FAIXA=="4"?'active':''}">faixa 4</div>
                    <div class="escala-5 ${d["1"].FAIXA=="5"?'active':''}">faixa 5</div>
                </div>
            </div>`;
            if(mapApp.indicator>1)o+=`<div>
                <p class="label">${mapApp.lente.LENTE} <b>${toFixed(d[mapApp.indicator].VALOR_PADRONIZADO,mapApp.lente.DECIMAIS)}</b></p>
                <div class="escala">
                    <div class="escala-0">faixa 0</div>
                    <div class="escala-1 ${d[mapApp.indicator].FAIXA=="1"?'active':''}">faixa 1</div>
                    <div class="escala-2 ${d[mapApp.indicator].FAIXA=="2"?'active':''}">faixa 2</div>
                    <div class="escala-3 ${d[mapApp.indicator].FAIXA=="3"?'active':''}">faixa 3</div>
                    <div class="escala-4 ${d[mapApp.indicator].FAIXA=="4"?'active':''}">faixa 4</div>
                    <div class="escala-5 ${d[mapApp.indicator].FAIXA=="5"?'active':''}">faixa 5</div>
                </div>
            </div>`;
            o+=`<div class="ilustra-tema ${mapApp.lente.SLUG} mb1">
                <img src="${template_url}/assets/images/temas/${mapApp.lente.SLUG}-ilustra.png">
            </div>
            ${d[mapApp.indicator].TEXTO?'<p class="legenda-recordista legenda">'+d[mapApp.indicator].TEXTO+'</p>':''}
        `;

        mapApp.mapcard.classList.add("active");
        mapApp.cardcontent.innerHTML = o;
        mapApp.zoom(t,a);
    },
    zoom: function(t,a){
        var all = mapApp.map._container.querySelectorAll('path.active');
        if(all) all.forEach(x=>{x.classList.remove('active');});
        if(t){
            mapApp.map.fitBounds(L.geoJson([t.feature]).getBounds().pad(0.1));
            t._path.classList.add('active');
            mapApp.zoommed = [a,t];
            //this.pane.classList.add('perspective');
        }else{
            if(mapApp.zoomLayer) mapApp.map.fitBounds(mapApp.zoomLayer.getBounds().pad(0.1));
            mapApp.zoommed = false;
            //this.pane.classList.remove('perspective');
            mapApp.mapcard.classList.remove("active");
        }
    }
}

// Controllers //
function start(){
    generalApp.init();
    var current_page = mainContent.dataset.page;
    switch(current_page){
        case 'home': 
            homeApp.init();
            break;
        case 'mapa': 
            mapApp.init();
            break;
        default: 
            break;
    }

    scrollWatch();
}

function ready(fn) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}
window.onload = start;
ready(start);
window.addEventListener('resize', start);
window.addEventListener("scroll", scrollWatch);
window.addEventListener("popstate", (e)=>{generalApp.changePage(window.location.href)});




