<?php
get_header(); 
?>

<main class="institucional animacao">
    <div class="container-fluid wrap">
        <h2>Animação</h2>
        <div class="video-wrapper">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/ECBcFxPeKww" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="mt2">
            <p class="w700 mb0">Os conflitos de terra em 5 minutos</p>
            <p class="mt0"><i>A narração é do premiado escritor Itamar Vieira Junior</i></p>
            <p>Nesta animação a gente te chama a refletir sobre como a soja e o gado têm a ver com violência, sobre como o agronegócio, a mineração e a produção de energia podem estar conectados a violentos conflitos no campo e sobre como a má política une todas essas questões que levam centenas de defensores como Dorothy Stang e Chico Mendes a morrerem na defesa da floresta.</p>
        </div>
    </div>
</main>

<?php get_footer(); ?>