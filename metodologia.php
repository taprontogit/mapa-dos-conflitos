<?php
get_header(); 
?>

<main class="institucional sobre">
    <div class="container-fluid wrap">

        <h2>Metodologia</h2>
        
        <p>Como são calculadas as pontuações de cada município.</p>
        
        <p class="mb05">A <strong>pontuação</strong> é calculada de três formas:</p>
        
        <ol>
            <li><strong>valor bruto</strong>: isso ocorre nas lentes água, conflitos e desigualdade;</li>
            <li>proporcionalmente ao tamanho da <strong>população</strong> do município: lentes agrotóxicos e violência;</li>
            <li>proporcionalmente ao tamanho da <strong>área</strong> do município: lentes desmatamento, queimadas e mineração.</li>
        </ol>

        <div>
            <h3>Conflitos</h3>
            <p>A quantidade de conflitos por município foi obtida através do levantamento da CPT. Na base de dados da CPT, um mesmo conflito pode envolver mais de um município; desse modo, foram contabilizados os municípios em cada ocorrência de conflitos. Para a divisão das faixas, foi considerado o valor bruto de conflitos, sem padronização em relação à área ou população.</p>

            <h3>IDH-M</h3>
            <p>O último dado atualizado sobre o Índice de Desenvolvimento Humano Municipal é de 2010. Para a divisão das faixas, foi adaptada a classificação do Pnud - Programa das Nações Unidas para o desenvolvimento:</p>
            <table>
                <tbody>
                    <tr>
                        <td>IDH-M</td>
                        <td>Faixa</td>
                    </tr>
                    <tr>
                        <td>0 (sem informação)</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>0,001-0,499</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>0,500-0,599</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>0,600-0,699</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>0,700-0,799</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>0,800-1,000</td>
                        <td>1</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div>
            <h3>Mineração</h3>
            <p>Os processos minerários foram extraídos dos dados abertos da ANM, agregados por município e ano e padronizados de acordo com a fórmula:</p>
            <div class="equacao">
                <p>Pontuação <sub>mineração</sub> = </p> 
                <p class="fracao"><sup>processos</sup><sub>área</sub></p> 
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Queimadas</h3>
            <p>A quantidade de focos por município foi calculada a partir dos dados da plataforma <a href="https://queimadas.dgi.inpe.br/queimadas/bdqueimadas">BDQueimadas</a>, do Inpe. Consideramos os focos de incêndio na Amazônia Legal entre os anos 2011 e 2020, registrados pelo satélite de referência (AQUA-MT). Os dados foram agregados por município e ano, e padronizados de acordo com a fórmula:</p>
            <div class="equacao">
                <p>Pontuação <sub>queimadas</sub> = </p> 
                <p class="fracao"><sup>focos</sup><sub>área</sub></p> 
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Desmatamento</h3>
            <p>Os dados de desmatamento foram obtidos no sistema <a href="http://www.dpi.inpe.br/prodesdigital/prodesmunicipal.php" target="_blank">Prodes</a>, do Inpe. Foram exportados os dados de incremento do desmatamento por ano e por município, no período entre 2011 e 2020. O incremento de desmatamento foi padronizado de acordo com a área do município, conforme a fórmula:</p>
            <div class="equacao">
                <p>Pontuação <sub>desmatamento</sub> = </p> 
                <p class="fracao"><sup>incremento</sup><sub>área</sub></p>
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Agrotóxicos</h3>
            <p>Os dados são de notificações de intoxicação exógena registradas no Sinan-Net e foram obtidos pela plataforma <a href="https://datasus.saude.gov.br/acesso-a-informacao/doencas-e-agravos-de-notificacao-de-2007-em-diante-sinan/" target="_blank">Tabnet — DataSUS</a>, do Ministério da Saúde. Os filtros aplicados para a coleta foram:</p>
            <ul>
                <li>notificações por município de residência e ano 1º(s) sintoma(s);</li>
                <li>agente tóxico: agrotóxico agrícola;</li>
                <li>circunstância: uso habitual, acidental, ambiental;</li>
                <li>classificação final: intoxicação confirmada;</li>
                <li>período: 2011-2020.</li>
            </ul>
            <p>As notificações foram padronizadas de acordo com o tamanho da população do município, conforme a fórmula:</p>
            <div class="equacao">
                <p>Pontuação <sub>agrotóxicos</sub> = </p> 
                <p class="fracao"><sup>notificações</sup><sub>população</sub></p> 
                <p> x 100000</p> 
            </div>
        </div>

        <div>
            <h3>Violência</h3>
            <p>Os dados foram obtidos pela plataforma <a href="https://datasus.saude.gov.br/acesso-a-informacao/doencas-e-agravos-de-notificacao-de-2007-em-diante-sinan/" target="_blank">Tabnet — DataSUS</a>, do Ministério da Saúde. Os filtros aplicados para a coleta foram:</p>
            <ul>
                <li>morbidade hospitalar do SUS por causas externas;</li>
                <li>internações por município de residência e ano;</li>
                <li>grande grupo de causas: X85-Y09 agressões;</li>
                <li>período: 2011-2020.</li>
            </ul>
            <p>As notificações foram padronizadas de acordo com o tamanho da população do município, conforme a fórmula:</p>
            <div class="equacao">
                <p>Pontuação <sub>violência</sub> = </p> 
                <p class="fracao"><sup>notificações</sup><sub>população</sub></p> 
                <p> x 100000</p> 
            </div>
        </div>

        <div>
            <h3>Água</h3>
            <p>Os dados de outorgas de uso de água foram baixados dos dados abertos da Agência Nacional de Águas, agregados por município e ano. Foram filtradas apenas as outorgas com finalidade de direito de uso.</p>
        </div>

        <div>
            <h3>Faixas</h3>
            <p>Cada lente é dividida em faixas, que vão de 1 (mais baixa) a 5 (mais alta). Há ainda o valor 0, para municípios sem informação ou com nenhuma ocorrência na lente escolhida. Exceto a lente Desigualdade (já descrita), a divisão por faixas seguiu os critérios abaixo:</p>
            <table>
                <tbody>
                    <tr>
                        <td>Faixa</td>
                        <td>Ponto de corte</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>valores acima do Q3 + 3*IIQ (outliers, identificados por este <a href="https://github.com/biamuniz/mapadosconflitos/blob/main/code/id_outliers.Rmd">script</a>)</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>acima de 3*(Q3/4) até Q3</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>acima de 2*(Q3/4) até 3*(Q3/4)</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>acima de Q3/4 até 2*(Q3/4)</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>acima de 0 até Q3/4</td>
                    </tr>
                    <tr>
                        <td>0</td>
                        <td>sem informação/nenhuma ocorrência</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div>
            <h3>Dicionário de dados</h3>
            <ul>
                <li>ID: identificador único do registro/linha, com 5 dígitos;</li>
                <li>LENTE: categoria a ser destacada;</li>
                <li>CD_MUN: identificador do município, com 6 dígitos, de acordo com o IBGE;</li>
                <li>UF: unidade federativa;</li>
                <li>NM_MUN: nome do município;</li>
                <li>ANO: ano da ocorrência;</li>
                <li>VALOR: quantidade de ocorrências, notificações etc.;</li>
                <li>VALOR_PADRONIZADO: coluna “VALOR” após a padronização em relação à área ou população, usado para o cálculo das faixas;</li>
                <li>FAIXAS: índice de 0 a 5, calculado de acordo com o “VALOR_PADRONIZADO”;</li>
                <li>TEXTO: texto que destaca alguma característica do município de acordo com a lente.</li>
            </ul>
        </div>

    <div class="mt3">

            <!-- Begin Mailchimp Signup Form -->

        <div id="mc_embed_signup">
            <form action="https://apublica.us8.list-manage.com/subscribe/post?u=47bdda836f3b890e13c9f416d&amp;id=b7667a23e7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                <h3>Receba a base de dados do Mapa dos Conflitos</h3>
            <div class="mc-field-group">
                <label for="mce-EMAIL">E-mail  <span class="asterisk">*</span>
            </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="mc-field-group">
                <label for="mce-FNAME">Nome  <span class="asterisk">*</span>
            </label>
                <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
                <label for="mce-LNAME">Sobrenome </label>
                <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_47bdda836f3b890e13c9f416d_b7667a23e7" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"></div>
                </div>
            </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone'; /**
            * Translated default messages for the $ validation plugin.
            * Locale: PT_PT
            */
            $.extend($.validator.messages, {
                required: "Campo de preenchimento obrigat&oacute;rio.",
                remote: "Por favor, corrija este campo.",
                email: "Por favor, introduza um endere&ccedil;o eletr&oacute;nico v&aacute;lido.",
                url: "Por favor, introduza um URL v&aacute;lido.",
                date: "Por favor, introduza uma data v&aacute;lida.",
                dateISO: "Por favor, introduza uma data v&aacute;lida (ISO).",
                number: "Por favor, introduza um n&uacute;mero v&aacute;lido.",
                digits: "Por favor, introduza apenas d&iacute;gitos.",
                creditcard: "Por favor, introduza um n&uacute;mero de cart&atilde;o de cr&eacute;dito v&aacute;lido.",
                equalTo: "Por favor, introduza de novo o mesmo valor.",
                accept: "Por favor, introduza um ficheiro com uma extens&atilde;o v&aacute;lida.",
                maxlength: $.validator.format("Por favor, n&atilde;o introduza mais do que {0} caracteres."),
                minlength: $.validator.format("Por favor, introduza pelo menos {0} caracteres."),
                rangelength: $.validator.format("Por favor, introduza entre {0} e {1} caracteres."),
                range: $.validator.format("Por favor, introduza um valor entre {0} e {1}."),
                max: $.validator.format("Por favor, introduza um valor menor ou igual a {0}."),
                min: $.validator.format("Por favor, introduza um valor maior ou igual a {0}.")
            });}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->

        </div>
   
    </div>
</main>

<?php get_footer(); ?>