<?php
get_header(); 
?>

<main class="institucional sobre">
    <div class="container-fluid wrap">
        <h2>Sobre</h2>
        <p>O Mapa dos Conflitos é um projeto da <strong>Agência Pública de Jornalismo Investigativo</strong> em parceria com a <strong>Comissão Pastoral da Terra (CPT)</strong> que, por meio de análise inédita de dados, investiga as ocorrências de conflitos no campo na Amazônia Legal na última década (2011-2020). </p>
        
        <p>Neste especial, foram analisados os conflitos no campo em comparação aos dados públicos relacionados a outras temáticas socioambientais como desmatamento, queimadas, mineração, água, agrotóxicos, violência e desigualdade.</p>
        
        <p>Essas ocorrências de conflitos no campo são informações coletadas e trazidas ao público anualmente nos relatórios de Conflitos no Campo da CPT, ação pastoral da Igreja Católica e única entidade a realizar tão ampla pesquisa sobre os conflitos no campo em âmbito nacional. </p>
        
        <p>A definição de conflitos no campo se refere às ações de resistência e enfrentamento que ocorrem em diferentes contextos sociais no âmbito rural, envolvendo a luta pela terra, água e direitos e pelos meios de trabalho ou produção. Esses conflitos se dão entre classes sociais, entre os trabalhadores ou por causa da ausência ou da má gestão de políticas públicas.</p>

        <p>Com esse trabalho de décadas formou-se um dos mais importantes acervos documentais sobre as lutas pela terra-território e formas de resistência dos trabalhadores e trabalhadoras da terra, das águas e das florestas, bem como sobre a defesa e conquista de direitos. </p>

        <p>Já a Pública é a primeira agência de jornalismo investigativo sem fins lucrativos do Brasil e foi criada por repórteres mulheres em 2011. Nossas reportagens investigativas são pautadas pelo interesse público e feitas com base na rigorosa apuração dos fatos e na defesa intransigente dos direitos humanos. Em reconhecimento a esse trabalho, é a agência de notícias mais premiada no Brasil de acordo com o ranking da Jornalistas & Cia.</p>

        <p>Há 11 anos investigamos a questão da propriedade da terra na Amazônia e a resistência dos povos indígenas, ribeirinhos e agricultores familiares contra a espoliação e a expulsão das comunidades de seu território. </p>

        <p>O Mapa dos Conflitos faz parte do especial <a href="https://apublica.org/especial/amazonia-sem-lei-2021/" target="_blank">“Amazônia sem lei”</a>, que investiga a violência relacionada à regularização fundiária, à demarcação de terras e à reforma agrária na Amazônia Legal e no Cerrado.</p>
    
        <div class="contato">
            <p class="w700">Contato: </p>
            <p>informações via <a href="https://apublica.org/contato/" target="_blank">formulário</a></p>
            <p>e-mail: <a href="mailto:redacao@apublica.org" target="_blank">redação@apublica.org</a></p>
        </div>
    </div>
</main>

<?php get_footer(); ?>