<footer class="footer">
    <div class="wrap container-fluid">
        <div class="footer-inner">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-7 col-xs-12">
                            <img class="pb3 logo" src="<?php tu(); ?>/assets/images/logo-full.svg">
                            <p class="tag">Siga nossas redes</p>
                            <div class="social">
                                <a href="#" class="twitter">twitter</a>
                                <a href="#" class="instagram">instagram</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-5 col-xs-12">
                            <p class="tag">Site</p>
                            <?php get_template_part('blocks/_links-nav'); ?> 
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-7 col-xs-12">
                    <p class="tag">Assessoria de imprensa</p>
                    <div class="row">
                        <div class="col-md-6 col-sm-4 col-xs-12">
                            <p class="w700 mb0">Chico Damaso</p>
                            <p class="mt0">(11) 99911 8117</p>
                        </div>
                        <div class="col-md-6 col-sm-5 col-xs-12">
                            <p class="w700 mb0">Andrezza Ferrigno</p>
                            <p class="mt0">(11) 99982 7007</p>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <a class="mb2" href="#">acontece@acontecenoticias.com.br</a>
                            <p class="tag mt2">Dúvidas, parcerias e contato em geral</p>
                            <a class="mb2" href="#">observatorioobstetricobr@gmail.com</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <p class="tag">Assine nossa newsletter</p>
                    <form>
                        <input type="email" placeholder="e-mail">
                        <input type="text" placeholder="área de atuação">
                        <button class="btn">enviar</button>
                    </form>
                </div>

            </div>
            <a href="<?php echo home_url(); ?>" class="logo"></a>                   
        </div>            
    </div>             
</footer>