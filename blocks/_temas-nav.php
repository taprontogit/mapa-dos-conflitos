<div class="nav-temas nav-mapa">
    <div>
        <a onmouseenter="play('hover');" onclick="play('click');mapApp.indicator=1;mapApp.filter();" data-indicator="1" class="tema conflitos"><span></span></a>
        <div class="tooltip left top">
            <h3>Conflitos</h3>
            <img src="<?php tu(); ?>/assets/images/temas/conflitos-lente.png">
        </div>
    </div>
    <div>
        <a onmouseenter="play('hover');" onclick="play('click');mapApp.indicator=6;mapApp.filter();" data-indicator="6" class="tema queimadas"><span></span></a>
        <div class="tooltip left top">
            <h3>Queimadas</h3>
            <img src="<?php tu(); ?>/assets/images/temas/queimadas-lente.png">
        </div>
    </div>
    <div>
        <a onmouseenter="play('hover');" onclick="play('click');mapApp.indicator=4;mapApp.filter();" data-indicator="4" class="tema desmatamento"><span></span></a>
        <div class="tooltip left top">
            <h3>Desmatamento</h3>
            <img src="<?php tu(); ?>/assets/images/temas/desmatamento-lente.png">
        </div>
    </div>
    <div>
        <a onmouseenter="play('hover');" onclick="play('click');mapApp.indicator=2;mapApp.filter();" data-indicator="2" class="tema agrotoxicos"><span></span></a>
        <div class="tooltip left top">
            <h3>Agrotóxicos</h3>
            <img src="<?php tu(); ?>/assets/images/temas/agrotoxicos-lente.png">
        </div>
    </div>
    <div>
        <a onmouseenter="play('hover');" onclick="play('click');mapApp.indicator=8;mapApp.filter();" data-indicator="8" class="tema desigualdade"><span></span></a>
        <div class="tooltip right top">
            <h3>Desigualdade</h3>
            <img src="<?php tu(); ?>/assets/images/temas/desigualdade-lente.png">
        </div>
    </div>
    <div>
        <a onmouseenter="play('hover');" onclick="play('click');mapApp.indicator=3;mapApp.filter();" data-indicator="3" class="tema agua"><span></span></a>
        <div class="tooltip right top">
            <h3>Água</h3>
            <img src="<?php tu(); ?>/assets/images/temas/agua-lente.png">
        </div>
    </div>
    <div>
        <a onmouseenter="play('hover');" onclick="play('click');mapApp.indicator=5;mapApp.filter();" data-indicator="5" class="tema mineracao"><span></span></a>
        <div class="tooltip right top">
            <h3>Mineração</h3>
            <img src="<?php tu(); ?>/assets/images/temas/mineracao-lente.png">
        </div>
    </div>
    <div>
        <a onmouseenter="play('hover');" onclick="play('click');mapApp.indicator=7;mapApp.filter();" data-indicator="7" class="tema violencia"><span></span></a>
        <div class="tooltip right top">
            <h3>Violência</h3>
            <img src="<?php tu(); ?>/assets/images/temas/violencia-lente.png">
        </div>
    </div>
</div>
