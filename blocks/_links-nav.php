<li><a class="<?php if($current_page=='mapa') echo 'current'; ?>" href="<?php echo home_url('mapa'); ?>">Mapa</a></li>
<li><a class="<?php if($current_page=='sobre') echo 'current'; ?>" href="<?php echo home_url('sobre'); ?>">Sobre</a></li>
<li><a class="<?php if($current_page=='metodologia') echo 'current'; ?>" href="<?php echo home_url('metodologia'); ?>">Metodologia</a></li>
<li><a class="<?php if($current_page=='reportagens') echo 'current'; ?>" href="<?php echo home_url('reportagens'); ?>">Reportagens</a></li>
<li><a class="<?php if($current_page=='animacao') echo 'current'; ?>" href="<?php echo home_url('animacao'); ?>">Animação</a></li>  
<li><a class="<?php if($current_page=='duvidas-frequentes') echo 'current'; ?>" href="<?php echo home_url('duvidas-frequentes'); ?>">Dúvidas Frequentes</a></li>             
<li><a class="<?php if($current_page=='expediente') echo 'current'; ?>" href="<?php echo home_url('expediente'); ?>">Expediente</a></li>