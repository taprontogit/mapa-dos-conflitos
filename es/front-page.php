<?php
$current_page = 'home';
get_header(); 
?>

<main id="intro">
    <div class="intro-section intro-1">
        <div class="center">
            <div class="logoglitch"><img src="<?php tu(); ?>/assets/images/main-logo.svg" alt="Mapa dos Conflitos"><img class="lglitch" src="<?php tu(); ?>/assets/images/main-logo-c.svg" alt="Mapa dos Conflitos"></div>
            <p>Mapa de conflictos: una década de violencia e injusticia por la tierra en la Amazonia Legal </p>
        </div>
        <div class="apoiadores">
            <img src="<?php tu(); ?>/assets/images/publica.png">
            <img src="<?php tu(); ?>/assets/images/cpt.png">
        </div>
    </div>
    <div class="intro-section intro-2">
        <div class="center">
            <div>
                <h2>Amazonia legal</h2>
                <p>El Mapa de Conflictos centra su análisis en la Amazonia Legal, una región que abarca más de 5 millones de km2, lo que representa dos tercios del país. La zona incluye los estados de Amazonas, Roraima, Rondônia, Pará, Amapá, Acre, Tocantins, Mato Grosso y parte de Maranhão. Es en la Amazonía Legal donde se concentra el 55% de los conflictos en el campo entre 2011 y 2020.</p>
            </div>
        </div>
    </div>
    <div class="intro-section intro-big">
        <div class="center">
            <div>
                <div class="frases">
                    <h2>La última década en el Amazonas</h2>
                    <span class="data">(2011-2020)</span>
                    <h4><strong>+ de 300</strong> asesinatos</h4>
                    <h4><strong>+ de 2 mil</strong> víctimas</h4>
                    <h4><strong>+ de 100 mil</strong> familias afectadas</h4>
                    <h4><strong>+ de 7 mil</strong> incidencias de conflictos</h4>
                    <h4><strong>9</strong> estados y <strong>583</strong> municipios afectados</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-section conflitos-section">
        <div class="center">
            <div style="overflow-x: hidden;">
                <?php get_template_part('blocks/_temas-intro'); ?>
                <div class="fixpos">
                    <div class="infos-geral">
                        <h2>Mapa de conflictos</h2>
                        <p>Navegue por las lentes temáticas y descubra cómo los conflictos en el campo están relacionados con la deforestación, los incendios, la minería, el agua, los pesticidas, la violencia y la desigualdad. Vea lo que representa cada objetivo a continuación.</p>
                    </div>
                    <div class="infos-tema" data-indicador="1">
                        <h2>Conflictos</h2>
                        <p>Las incidencias de conflictos en el campo son informaciones recopiladas anualmente por el CPT y sistematizadas por el Centro de Documentación Dom Tomás Balduino – CEDOC.</p>
                    </div>
                    <div class="infos-tema" data-indicador="6">
                        <h2>Quema</h2>
                        <p>Los focos de quema son medidos por el INPE. Esta lente muestra la cantidad de focos quemados en relación con la superficie del municipio.</p>
                    </div>
                    <div class="infos-tema" data-indicador="4">
                        <h2>Deforestación</h2>
                        <p>Cuanto más alta es la clasificación, mayor es el avance de la deforestación en el municipio, medido por el Instituto Nacional de Investigaciones Espaciales (INPE). La lente considera el tamaño del municipio para el cálculo.</p>
                    </div>
                    <div class="infos-tema" data-indicador="2">
                        <h2>Plaguicidas</h2>
                        <p>La lente muestra el número de registros de intoxicaciones causadas por plaguicidas de uso agrícola. Las intoxicaciones se producen por accidentes durante la manipulación de plaguicidas, el trabajo o la pulverización.</p>
                    </div>
                    <div class="infos-tema" data-indicador="8">
                        <h2>Desigualdad</h2>
                        <p>Aquí se comparan los datos que reflejan la calidad de vida de la población según el Índice de Desarrollo Humano Municipal (IDH-M): la esperanza de vida, los años de escolarización y la renta per cápita.</p>
                    </div>
                    <div class="infos-tema" data-indicador="3">
                        <h2>Agua</h2>
                        <p>Para utilizar el agua de los ríos, lagos y otras fuentes para los cultivos y las industrias, es necesario que los gobiernos concedan permisos de agua. Esta lente muestra el número de permisos para el uso del agua en los municipios.</p>
                    </div>
                    <div class="infos-tema" data-indicador="5">
                        <h2>Minería</h2>
                        <p>Para poder explotar legalmente una mina, hay que solicitarlo a la Agencia Nacional de Minería (ANM). Esta lente muestra el número de estas solicitudes por municipio, teniendo en cuenta su tamaño.</p>
                    </div>
                    <div class="infos-tema" data-indicador="7">
                        <h2>Violencia</h2>
                        <p>La lente muestra el número de ingresos hospitalarios después de que las víctimas hayan sufrido agresiones de diversos tipos. El cálculo tiene en cuenta el tamaño de la población del municipio.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-section fim">
    </div>

    <div class="scrollTo"><span></span></div>
    <a href="<?php echo home_url('mapa'); ?>" class="btn">saltar la introducción</a>
    <div class="vinheta"></div>
</main>

<?php get_footer(); ?>