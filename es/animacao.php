<?php
get_header(); 
?>

<main class="institucional animacao">
    <div class="container-fluid wrap">
        <h2>Animación</h2>
        <div class="video-wrapper">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/ECBcFxPeKww" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="mt2">
            <p class="w700 mb0">Conflictos territoriales en 5 minutos</p>
            <p class="mt0"><i>La narración corre a cargo del premiado escritor Itamar Vieira Junior</i></p>
            <p>En esta animación te invitamos a reflexionar sobre cómo la soja y el ganado tienen que ver con la violencia, cómo el agronegocio, la minería y la producción de energía pueden estar relacionados con los conflictos violentos en el campo y cómo la mala política une todas estas cuestiones que llevan a cientos de defensores como Dorothy Stang y Chico Mendes a morir defendiendo la selva.</p>
        </div>
    </div>
</main>

<?php get_footer(); ?>