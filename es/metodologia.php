<?php
get_header(); 
?>

<main class="institucional sobre">
    <div class="container-fluid wrap">

        <h2>Metodología</h2>
        
        <p>Cómo se calculan las puntuaciones de cada municipio.</p>
        
        <p class="mb05">La <strong>puntuación</strong> se calcula de tres maneras:</p>
        
        <ol>
            <li><strong>valor bruto</strong>: se da en las lentes de agua, conflicto y desigualdad;</li>
            <li>en proporción al tamaño de la <strong>población</strong> del municipio: los agrotóxicos y los lentes de violencia;</li>
            <li>proporcionalmente al tamaño de la <strong>zona</strong> del municipio: deforestación, incendios y objetivos mineros.</li>
        </ol>

        <div>
            <h3>Conflictos</h3>
            <p>La cantidad de conflictos por municipio se obtuvo a través de la encuesta CPT. En la base de datos del CPT, un mismo conflicto puede implicar a más de un municipio; por lo tanto, se contaron los municipios en cada aparición de conflictos. Para la división de los rangos, se consideró el valor bruto de los conflictos, sin estandarización en relación con la superficie o la población.</p>

            <h3>HDI-M</h3>
            <p>Los últimos datos actualizados del Índice de Desarrollo Humano Municipal son de 2010. Para la división de los rangos, se adaptó la clasificación del PNUD - Programa de las Naciones Unidas para el Desarrollo:</p>
            <table>
                <tbody>
                    <tr>
                        <td>HDI-M</td>
                        <td>Gama</td>
                    </tr>
                    <tr>
                        <td>0 (sin información)</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>0,001-0,499</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>0,500-0,599</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>0,600-0,699</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>0,700-0,799</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>0,800-1,000</td>
                        <td>1</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div>
            <h3>Minería</h3>
            <p>Los procesos de extracción se extrajeron de los datos abiertos de la ANM, se agregaron por municipio y año y se normalizaron según la fórmula:</p>
            <div class="equacao">
                <p>Puntuación <sub>deforestación</sub> = </p> 
                <p class="fracao"><sup>incremento</sup><sub>área</sub></p> 
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Quema</h3>
            <p>El número de incendios por municipio se calculó a partir de los datos de la plataforma <a href="https://queimadas.dgi.inpe.br/queimadas/bdqueimadas">BDQueimadas</a>, del Inpe. Consideramos los focos de incendio en la Amazonia Legal entre los años 2011 y 2020, registrados por el satélite de referencia (AQUA-MT).</p>
            <p>Los datos se agregaron por municipio y año, y se normalizaron según la fórmula:</p>
            <div class="equacao">
                <p>Puntuación <sub>quema</sub> = </p> 
                <p class="fracao"><sup>focos</sup><sub>área</sub></p> 
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Deforestación</h3>
            <p>Los datos de deforestación se obtuvieron en el sistema <a href="http://www.dpi.inpe.br/prodesdigital/prodesmunicipal.php" target="_blank">Prodes</a>, del Inpe. Los datos de incremento de la deforestación se exportaron por año y por municipio, para el periodo entre 2011 y 2020. El incremento de la deforestación se estandarizó en función de la superficie del municipio, según la fórmula:</p>
            <div class="equacao">
                <p>Puntuación <sub>Deforestación</sub> = </p> 
                <p class="fracao"><sup>incremento</sup><sub>área</sub></p>
                <p> x 1000</p> 
            </div>
        </div>

        <div>
            <h3>Plaguicidas</h3>
            <p>Los datos proceden de las notificaciones de intoxicación exógena registradas en Sinan-Net y se obtuvieron de la plataforma <a href="https://datasus.saude.gov.br/acesso-a-informacao/doencas-e-agravos-de-notificacao-de-2007-em-diante-sinan/" target="_blank">Tabnet — DataSUS</a> del Ministerio de Sanidad. Los filtros aplicados para la recogida fueron:</p>
            <ul>
                <li>notificaciones por municipio de residencia y año 1er síntoma;</li>
                <li>agente tóxico: plaguicida agrícola</li>
                <li>circunstancia: uso habitual, accidental, ambiental;</li>
                <li>clasificación final: intoxicación confirmada;</li>
                <li>período: 2011-2020.</li>

            </ul>
            <p>Las notificaciones se normalizaron en función del tamaño de la población del municipio, según la fórmula:</p>
            <div class="equacao">
                <p>Puntuación <sub>Plaguicidas</sub> = </p> 
                <p class="fracao"><sup>notificaciones</sup><sub>población</sub></p> 
                <p> x 100000</p> 
            </div>
        </div>

        <div>
            <h3>Violencia</h3>
            <p>Los datos fueron obtenidos por la plataforma <a href="https://datasus.saude.gov.br/acesso-a-informacao/doencas-e-agravos-de-notificacao-de-2007-em-diante-sinan/" target="_blank">Tabnet — DataSUS</a>, del Ministerio de Sanidad. Los filtros aplicados para la recogida fueron:</p>
            <ul>
                <li>Morbilidad hospitalaria del SUS por causas externas;</li>
                <li>hospitalizaciones por municipio de residencia y año;</li>
                <li>grupo de causas principales: agresiones X85-Y09;</li>
                <li>período: 2011-2020.</li>
            </ul>
            <p>Las notificaciones se estandarizaron en función del tamaño de la población del municipio según la fórmula:</p>
            <div class="equacao">
                <p>Puntuación <sub>Violencia</sub> = </p> 
                <p class="fracao"><sup>notificaciones</sup><sub>población</sub></p> 
                <p> x 100000</p> 
            </div>
        </div>

        <div>
            <h3>Agua</h3>
            <p>Los datos de permisos de uso de agua se descargaron de los datos abiertos de la Agencia Nacional del Agua, agregados por municipio y año. Solo se filtraron las concesiones con la finalidad de derecho de uso.</p>
        </div>

        <div>
            <h3>BANDAS</h3>
            <p>Cada lente se divide en bandas, que van de 1 (la más baja) a 5 (la más alta). También existe el valor 0, para los municipios sin información o sin aparición en la lente elegida. Salvo en el caso de la lente de la desigualdad (ya descrita), la división por rangos siguió los criterios que se indican a continuación:</p>
            <table>
                <tbody>
                    <tr>
                        <td>Rango</td>
                        <td>Ponto de corte</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>valores por encima de Q3 + 3*IIQ (valores atípicos, identificados por este <a href="https://github.com/biamuniz/mapadosconflitos/blob/main/code/id_outliers.Rmd">script</a>)</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>por encima de 3*(Q3/4) hasta Q3</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>por encima de 2*(Q3/4) a 3*(Q3/4)</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>mayor que Q3/4 a 2*(Q3/4)</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>sobre 0 hasta Q3/4</td>
                    </tr>
                    <tr>
                        <td>0</td>
                        <td>sin información/sin ocurrencia</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div>
            <h3>Diccionario de datos</h3>
            <ul>
            <li>ID: identificador &uacute;nico del registro/fila, con 5 d&iacute;gitos;</li>
            <li>SLOW: categor&iacute;a a destacar;</li>
            <li>CD_MUN: identificador del municipio, con 6 d&iacute;gitos, seg&uacute;n el IBGE;</li>
            <li>UF: unidad federativa;</li>
            <li>NM_MUN: nombre del municipio;</li>
            <li>A&Ntilde;O: a&ntilde;o de la ocurrencia;</li>
            <li>VALOR: cantidad de ocurrencias, notificaciones, etc;</li>
            <li>VALOR_PADRONIZADO: columna "VALOR" despu&eacute;s de la normalizaci&oacute;n en relaci&oacute;n con la zona o la poblaci&oacute;n, utilizada para el c&aacute;lculo de los rangos;</li>
            <li>RANGO: &iacute;ndice de 0 a 5, calculado seg&uacute;n el "VALOR_ESTANDARIZADO";</li>
            <li>TEXTO: texto que destaca alguna caracter&iacute;stica del municipio seg&uacute;n el objetivo.</li>
            </ul>
        </div>

        <div class="mt3">

            <!-- Begin Mailchimp Signup Form -->

            <div id="mc_embed_signup">
            <form action="https://apublica.us8.list-manage.com/subscribe/post?u=47bdda836f3b890e13c9f416d&amp;id=b7667a23e7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                <h3>Recibir la base de datos del Mapa de Conflictos</h3>
            <div class="mc-field-group">
                <label for="mce-EMAIL">E-mail  <span class="asterisk">*</span>
            </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="mc-field-group">
                <label for="mce-FNAME">Nombre  <span class="asterisk">*</span>
            </label>
                <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
                <label for="mce-LNAME">Apellido </label>
                <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_47bdda836f3b890e13c9f416d_b7667a23e7" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"></div>
                </div>
            </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone'; /**
            * Translated default messages for the $ validation plugin.
            * Locale: PT_PT
            */
            $.extend($.validator.messages, {
                required: "Campo requerido",
                remote: "Por favor, corrija este campo.",
                email: "Por favor, introduce una dirección de correo electrónico válida",
                url: "Por favor, introduza um URL v&aacute;lido.",
                date: "Por favor, introduza uma data v&aacute;lida.",
                dateISO: "Por favor, introduza uma data v&aacute;lida (ISO).",
                number: "Por favor, introduza um n&uacute;mero v&aacute;lido.",
                digits: "Por favor, introduza apenas d&iacute;gitos.",
                creditcard: "Por favor, introduza um n&uacute;mero de cart&atilde;o de cr&eacute;dito v&aacute;lido.",
                equalTo: "Por favor, introduza de novo o mesmo valor.",
                accept: "Por favor, introduza um ficheiro com uma extens&atilde;o v&aacute;lida.",
                maxlength: $.validator.format("Por favor, n&atilde;o introduza mais do que {0} caracteres."),
                minlength: $.validator.format("Por favor, introduza pelo menos {0} caracteres."),
                rangelength: $.validator.format("Por favor, introduza entre {0} e {1} caracteres."),
                range: $.validator.format("Por favor, introduza um valor entre {0} e {1}."),
                max: $.validator.format("Por favor, introduza um valor menor ou igual a {0}."),
                min: $.validator.format("Por favor, introduza um valor maior ou igual a {0}.")
            });}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->

            </div>

            </div>

        </div>
</main>

<?php get_footer(); ?>