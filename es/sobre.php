<?php
get_header(); 
?>

<main class="institucional sobre">
    <div class="container-fluid wrap">
        <h2>Acerca de</h2>
        <p>El Mapa de los Conflictos es un proyecto de la<strong> Ag&ecirc;ncia P&uacute;blica de Jornalismo Investigativo </strong>en colaboraci&oacute;n con la<strong> Comisi&oacute;n Pastoral de la Tierra (CPT) </strong>que, a trav&eacute;s de un an&aacute;lisis de datos sin precedentes, investiga la ocurrencia de conflictos en el campo en la Amazonia Legal en la &uacute;ltima d&eacute;cada (2011-2020).</p>
        <p>En este informe especial, se analizaron los conflictos en el campo en comparaci&oacute;n con los datos p&uacute;blicos relacionados con otras cuestiones socioambientales como la deforestaci&oacute;n, los incendios, la miner&iacute;a, el agua, los pesticidas, la violencia y la desigualdad.</p>
        <p>Estos sucesos de conflictos en el campo son informaci&oacute;n que se recoge y se hace p&uacute;blica anualmente en los Informes sobre Conflictos en el Campo de CPT, una acci&oacute;n pastoral de la Iglesia Cat&oacute;lica y la &uacute;nica entidad que realiza una investigaci&oacute;n tan amplia sobre los conflictos rurales en todo el pa&iacute;s.</p>
        <p>La definici&oacute;n de conflictos en el campo se refiere a las acciones de resistencia y confrontaci&oacute;n que se producen en diferentes contextos sociales en las zonas rurales, que implican la lucha por la tierra, el agua, los derechos y los medios de trabajo o producci&oacute;n. Estos conflictos se producen entre clases sociales, entre trabajadores o por la ausencia o mala gesti&oacute;n de las pol&iacute;ticas p&uacute;blicas.</p>
        <p>Con este trabajo a lo largo de d&eacute;cadas, se ha formado uno de los fondos documentales m&aacute;s importantes sobre las luchas por el territorio y las formas de resistencia de los trabajadores de la tierra, el agua y los bosques, as&iacute; como sobre la defensa y la conquista de derechos.&nbsp;</p>
        <p><strong>P&uacute;blica</strong> es la primera agencia de periodismo de investigaci&oacute;n sin &aacute;nimo de lucro de Brasil y fue creada por mujeres reporteras en 2011. Nuestros reportajes de investigaci&oacute;n se gu&iacute;an por el inter&eacute;s p&uacute;blico y se basan en la investigaci&oacute;n rigurosa de los hechos y la defensa inflexible de los derechos humanos. En reconocimiento a esta labor, es la agencia de noticias m&aacute;s premiada de Brasil seg&uacute;n el ranking de Jornalistas &amp; Cia.</p>
        <p>Durante 11 a&ntilde;os hemos investigado la cuesti&oacute;n de la propiedad de la tierra en la Amazonia y la resistencia de los pueblos ind&iacute;genas, ribere&ntilde;os y agricultores familiares contra el expolio y la expulsi&oacute;n de las comunidades de su territorio.&nbsp;</p>
        <p>El Mapa de Conflictos forma parte del especial <a href="https://apublica.org/especial/amazonia-sem-lei-2021/" target="_blank">"Amazon&iacute;a sin ley"</a>, que investiga la violencia relacionada con la regularizaci&oacute;n de tierras, la demarcaci&oacute;n de tierras y la reforma agraria en la Amazon&iacute;a Legal y el Cerrado.</p>

        <div class="contato">
            <p class="w700">Contacto: </p>
            <p>información através del <a href="https://apublica.org/contato/" target="_blank">formulario</a></p>
            <p>correo electrónico: <a href="mailto:redacao@apublica.org" target="_blank">redação@apublica.org</a></p>
        </div>
    </div>
</main>

<?php get_footer(); ?>