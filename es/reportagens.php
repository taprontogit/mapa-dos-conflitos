<?php
get_header(); 
?>

<main class="institucional">
    <div class="container-fluid wrap">
        <h2>Informes</h2>
        <div class="row acenter">
            <div class="col-md-6 col-xs-12 mb2">
                <a href="https://apublica.org/2022/04/assassinatos-no-campo-em-2021-batem-recorde-dos-ultimos-quatro-anos/" target="_blank" class="reportagem">
                    <div class="zoomimg"><img src="<?php tu(); ?>/assets/images/r1.jpg"></div>
                    <h3>Asesinatos en el campo en 2021 rompen récord de los últimos cuatro años</h3>
                    <p>En el gobierno de Bolsonaro, el promedio de conflictos ya es el más alto de la historia. El año pasado fueron asesinadas 35 personas en el campo, 29 solo en la Amazonía.</p>
                </a>
                <a href="https://apublica.org/2022/04/assassinatos-no-campo-em-2021-batem-recorde-dos-ultimos-quatro-anos/" target="_blank" class="btn">ler informe</a>
            </div>
            <div class="col-md-6 col-xs-12 mb2">
                <a href="https://apublica.org/2022/04/mais-de-cem-mil-familias-foram-afetadas-em-uma-decada-de-conflitos-no-campo-na-amazonia/" target="_blank" class="reportagem">
                    <div class="zoomimg"><img src="<?php tu(); ?>/assets/images/r2.jpg"></div>
                    <h3 class="pb1">Más de 100 familias han sido afectadas en una década de conflicto en el campo amazónico</h3>
                    <p>Mapa de conflictos, herramienta exclusiva lanzada por Agência Pública y CPT, mapas de conflictos y datos socioambientales de todos los municipios de la Amazonía Legal</p>
                </a>
                <a href="https://apublica.org/2022/04/mais-de-cem-mil-familias-foram-afetadas-em-uma-decada-de-conflitos-no-campo-na-amazonia/" target="_blank" class="btn">ler informe</a>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>