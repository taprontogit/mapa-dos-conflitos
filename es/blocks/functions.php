<?php 
    define("HOME", str_replace("\\",'/',(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://".$_SERVER['HTTP_HOST'].substr(getcwd(),strlen($_SERVER['DOCUMENT_ROOT']))));
    define("MAINFILE", isset($_GET['pgfile']) && $_GET['pgfile'] ? strtolower($_GET['pgfile']):"home");
    //define("CDN", str_replace(['www.tapronto.com.br','tapronto.com.br'],'d8e9qfpf0n73d.cloudfront.net',HOME));

    function home_url($s=""){ // Nat
        return HOME.'/'.$s;
    }
    function tu($e=true){ // Nat
        if($e) echo HOME.'/../';
        return HOME.'/../';
    }
    function currentURL(){
        $param = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?'));
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$param}";
    }

    function cleanName($s){
        $s = strtr(utf8_decode($s), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿšÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝŠ'), 'aaaaaceeeeiiiinooooouuuuyysAAAAACEEEEIIIINOOOOOUUUUYS');
        $s = str_replace("'", '_', $s);
        $s = preg_replace("/[^A-Za-z \_\-]/", "", strtolower($s));
        $s = preg_replace("/\s+/", "_", $s);
        $s = preg_replace("/-+/", "-", $s);
        return $s;
    }
    function get_template_part($slug,$name=null,$args=array()){
        global $current_page;
        include "{$slug}.php";
    }
    function wp_redirect($s=""){
        header("Location: ".HOME.'/'.$s); exit;
    }
    function wp_footer(){}
    function wp_head(){}
    function get_header(){
        global $current_page, $topStyle;
        include_once 'header.php';
    }
    function get_footer(){
        global $current_page;
        include_once 'footer.php';
    }

    $current_page = isset($current_page) ? $current_page : MAINFILE;
    $topStyle = isset($topStyle) ? $topStyle : null;
