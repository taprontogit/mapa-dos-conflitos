<?php if(!isset($_GET['spajax'])): ?>
<!DOCTYPE html>
<html lang="pt_br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="language" content="pt-br">
    <title>Mapa de conflictos</title>
    <?php wp_head(); ?>
    <meta property="og:type" content="website">
    <meta property="og:title" content="Mapa dos Conflitos">
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:image" content="<?php tu(); ?>/assets/images/share-thumb.png">
    <meta property="og:image:type" content="image/png">
    <link rel="icon" type="image/png" href="<?php tu(); ?>/assets/images/favicon.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;400;500;600&family=Open+Sans&display=swap" rel="stylesheet">
    <link href="<?php tu(); ?>/assets/images/favicon.png" rel="shortcut icon">
    <link rel="stylesheet" href="<?php tu(); ?>/build/style.css"> 

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-29998232-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-29998232-1');
    </script>
</head>

<body>
    <div id="loader" style="
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        align-items: center;
        background-color: #1E1518;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        height: 100%;
        justify-content: center;
        left: 0;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 999999;
    ">
        <svg width="725" height="180" viewBox="0 0 725 180" fill="none" xmlns="http://www.w3.org/2000/svg" style="width: 70%; max-width: 300px; height: auto;">
            <style>
                @keyframes dash {
                    to {stroke-dashoffset: 0;}
                }
                .animelogo path{
                    stroke-dasharray: 500;
                    stroke-dashoffset: 500;
                    animation: dash 2s linear infinite;
                    animation-direction: alternate;
                }
                .animelogo path:nth-child(1){animation-delay: 100ms;}
                .animelogo path:nth-child(2){animation-delay: 200ms;}
                .animelogo path:nth-child(3){animation-delay: 300ms;}
                .animelogo path:nth-child(4){animation-delay: 400ms;}
                .animelogo path:nth-child(5){animation-delay: 500ms;}
                .animelogo path:nth-child(6){animation-delay: 600ms;}
                .animelogo path:nth-child(7){animation-delay: 700ms;}
                .animelogo path:nth-child(8){animation-delay: 800ms;}
                .animelogo path:nth-child(9){animation-delay: 900ms;}
                .animelogo path:nth-child(10){animation-delay: 1000ms;}
                .animelogo path:nth-child(11){animation-delay: 1100ms;}
                .animelogo path:nth-child(12){animation-delay: 1200ms;}
                .animelogo path:nth-child(13){animation-delay: 1300ms;}
                .animelogo path:nth-child(14){animation-delay: 1400ms;}
                .animelogo path:nth-child(15){animation-delay: 1500ms;}
                .animelogo path:nth-child(16){animation-delay: 1600ms;}
                .animelogo path:nth-child(17){animation-delay: 1700ms;}
                .animelogo path:nth-child(18){animation-delay: 1800ms;}
            </style>
            <g class="animelogo">
                <path d="M20.0664 0H3.0664C1.40955 0 0.0664062 1.34315 0.0664062 3V76.9999C0.0664062 78.6567 1.40955 79.9999 3.06641 79.9999H17.0664C18.7232 79.9999 20.0664 78.6567 20.0664 76.9999V39.9359C20.0664 36.9653 23.9211 35.8014 25.5651 38.2757L37.5093 56.2519C38.694 58.0349 41.3114 58.039 42.5016 56.2596L54.5727 38.213C56.2217 35.7478 60.0663 36.9151 60.0663 39.881V76.9999C60.0663 78.6567 61.4095 79.9999 63.0663 79.9999H77.0663C78.7231 79.9999 80.0663 78.6567 80.0663 76.9999V3C80.0663 1.34314 78.7231 0 77.0663 0H60.0663H20.0664Z" stroke="#FFF"/>
                <path d="M187.026 0C185.37 0 184.026 1.34315 184.026 3V76.9999C184.026 78.6567 185.37 79.9999 187.026 79.9999H201.026C202.683 79.9999 204.026 78.6567 204.026 76.9999V56.3333C204.026 54.6764 205.369 53.3333 207.026 53.3333H261.026C262.683 53.3333 264.026 51.9901 264.026 50.3333V39.9999V13.3333V3C264.026 1.34315 262.683 0 261.026 0H204.026H187.026ZM208.368 41.1624C206.373 42.1598 204.026 40.7093 204.026 38.4792V14.8541C204.026 12.6239 206.373 11.1735 208.368 12.1708L231.993 23.9833C234.204 25.0889 234.204 28.2443 231.993 29.3499L208.368 41.1624Z" stroke="#FFF"/>
                <path d="M537.066 79.9999C538.723 79.9999 540.066 78.6567 540.066 76.9999V53.3199V26.64V3C540.066 1.34315 538.723 0 537.066 0H480.066H463.066C461.41 0 460.066 1.34315 460.066 3V76.9999C460.066 78.6567 461.41 79.9999 463.066 79.9999H480.066H537.066ZM480.066 18.9383C480.066 16.5424 482.737 15.1132 484.73 16.4421L516.31 37.4916C518.091 38.6787 518.091 41.2954 516.311 42.4834L484.731 63.5539C482.738 64.8841 480.066 63.455 480.066 61.0584V18.9383Z" stroke="#FFF"/>
                <path d="M322.94 56H309.111C308.119 56 307.192 56.4904 306.633 57.31L303.224 62.31C301.866 64.3014 303.292 67 305.702 67H326.35C328.76 67 330.186 64.3014 328.828 62.31L325.419 57.31C324.86 56.4904 323.932 56 322.94 56Z" stroke="#FFF"/>
                <path d="M329.293 0H279.026C277.37 0 276.026 1.34315 276.026 3V70.0806C276.026 73.0497 279.878 74.2146 281.524 71.7431L313.503 23.7101C314.69 21.927 317.31 21.9268 318.497 23.7097L350.489 71.7462C352.135 74.2173 355.986 73.0522 355.986 70.0833V3C355.986 1.34314 354.643 0 352.986 0H329.293Z" stroke="#FFF"/>
                <path d="M138.981 56H125.152C124.161 56 123.233 56.4904 122.674 57.31L119.265 62.31C117.907 64.3014 119.333 67 121.743 67H142.391C144.801 67 146.227 64.3014 144.869 62.31L141.46 57.31C140.901 56.4904 139.973 56 138.981 56Z" stroke="#FFF"/>
                <path d="M145.333 0H95.0664C93.4096 0 92.0664 1.34315 92.0664 3V70.0806C92.0664 73.0497 95.9181 74.2146 97.5636 71.7431L129.543 23.7101C130.73 21.927 133.35 21.9268 134.537 23.7097L166.529 71.7462C168.175 74.2173 172.026 73.0522 172.026 70.0833V3C172.026 1.34314 170.683 0 169.026 0H145.333Z" stroke="#FFF"/>
                <path d="M629.04 79.9999C630.696 79.9999 632.04 78.6567 632.04 76.9999V53.3333V26.72V3C632.04 1.34315 630.696 0 629.04 0H605.346H555.066C553.41 0 552.066 1.34315 552.066 3V26.64V53.3599V76.9999C552.066 78.6567 553.41 79.9999 555.066 79.9999H605.333H629.04ZM567.529 42.1197C566.36 40.9484 566.36 39.0517 567.529 37.8807L589.931 15.4455C591.102 14.2721 593.004 14.2718 594.176 15.445L616.613 37.9048C617.784 39.0767 617.783 40.9758 616.612 42.1469L594.176 64.5709C593.004 65.7428 591.103 65.7416 589.932 64.5682L567.529 42.1197Z" stroke="#FFF"/>
                <path d="M682.072 19.0166C679.241 17.6009 680.248 13.3333 683.414 13.3333H720.972C722.629 13.3333 723.972 11.9902 723.972 10.3333V3C723.972 1.34315 722.629 0 720.972 0H647.039C645.382 0 644.039 1.34315 644.039 3V13.3333V38.145C644.039 39.2818 644.682 40.3209 645.698 40.8288L686.047 60.9827C688.879 62.3976 687.872 66.6666 684.706 66.6666H647.079C645.422 66.6666 644.079 68.0097 644.079 69.6666V76.9999C644.079 78.6567 645.422 79.9999 647.079 79.9999H721.039C722.696 79.9999 724.039 78.6567 724.039 76.9999V41.854C724.039 40.7177 723.397 39.6789 722.381 39.1708L682.072 19.0166Z" stroke="#FFF"/>
                <path d="M118.733 100H95.0664C93.4096 100 92.0664 101.343 92.0664 103V126.64V153.36V177C92.0664 178.657 93.4096 180 95.0664 180H145.333H169.026C170.683 180 172.026 178.657 172.026 177V153.333V126.72V103C172.026 101.343 170.683 100 169.026 100H145.333H118.733ZM134.176 164.571C133.004 165.743 131.103 165.742 129.932 164.569L107.516 142.12C106.347 140.948 106.347 139.051 107.516 137.88L129.918 115.445C131.089 114.272 132.991 114.271 134.163 115.444L156.612 137.905C157.783 139.077 157.783 140.976 156.611 142.147L134.176 164.571Z" stroke="#FFF"/>
                <path d="M578.839 100H555.173C553.516 100 552.173 101.343 552.173 103V126.64V153.36V177C552.173 178.657 553.516 180 555.173 180H605.439H629.133C630.79 180 632.133 178.657 632.133 177V153.333V126.72V103C632.133 101.343 630.79 100 629.133 100H605.439H578.839ZM594.282 164.57C593.11 165.743 591.209 165.742 590.038 164.569L567.622 142.119C566.453 140.948 566.453 139.052 567.622 137.88L590.024 115.433C591.196 114.259 593.097 114.258 594.27 115.431L616.719 137.892C617.89 139.064 617.89 140.963 616.719 142.134L594.282 164.57Z" stroke="#FFF"/>
                <path d="M3.06641 100C1.40955 100 0.0664062 101.343 0.0664062 103V153.333V177C0.0664062 178.657 1.40955 180 3.06641 180H70.158C73.1262 180 74.2918 176.15 71.8221 174.504L23.8106 142.496C22.0294 141.309 22.0294 138.691 23.8106 137.504L71.8221 105.496C74.2918 103.85 73.1262 100 70.158 100H3.06641Z" stroke="#FFF"/>
                <path d="M444.895 101.661C444.387 100.643 443.348 100 442.21 100H418.923C416.692 100 415.242 102.348 416.24 104.343L453.277 178.343C453.785 179.358 454.824 180 455.96 180H479.112C481.34 180 482.791 177.656 481.796 175.661L444.895 101.661Z" stroke="#FFF"/>
                <path d="M250.693 140.657C250.693 143.823 246.424 144.83 245.009 141.997L224.855 101.659C224.347 100.642 223.308 100 222.172 100H197.36H187.026C185.37 100 184.026 101.343 184.026 103V176.933C184.026 178.59 185.37 179.933 187.026 179.933H194.36C196.017 179.933 197.36 178.59 197.36 176.933V139.375C197.36 136.209 201.627 135.202 203.043 138.033L223.197 178.342C223.705 179.358 224.744 180 225.88 180H261.026C262.683 180 264.026 178.657 264.026 177V103.027C264.026 101.37 262.683 100.027 261.026 100.027H253.693C252.036 100.027 250.693 101.37 250.693 103.027V140.657Z" stroke="#FFF"/>
                <path d="M721.066 113.333C722.723 113.333 724.066 111.99 724.066 110.333V103C724.066 101.343 722.723 100 721.066 100H647.133C645.476 100 644.133 101.343 644.133 103V113.333V138.145C644.133 139.282 644.775 140.321 645.792 140.829L686.14 160.983C688.973 162.398 687.966 166.667 684.8 166.667H647.173C645.516 166.667 644.173 168.01 644.173 169.667V177C644.173 178.657 645.516 180 647.173 180H721.133C722.79 180 724.133 178.657 724.133 177V141.854C724.133 140.718 723.491 139.679 722.474 139.171L682.166 119.017C679.335 117.601 680.342 113.333 683.508 113.333H721.066Z" stroke="#FFF"/>
                <path d="M329.346 100L289.346 100H279.026C277.37 100 276.026 101.343 276.026 103V177C276.026 178.657 277.37 180 279.026 180H287.503C288.641 180 289.68 179.356 290.188 178.338L301.825 154.995C302.332 153.977 303.372 153.333 304.51 153.333H352.96C354.616 153.333 355.96 151.99 355.96 150.333V143C355.96 141.343 354.616 140 352.96 140H314.189C311.959 140 310.508 137.652 311.507 135.657L321.85 114.991C322.359 113.975 323.397 113.333 324.533 113.333H352.96C354.616 113.333 355.96 111.99 355.96 110.333V103C355.96 101.343 354.616 100 352.96 100L329.346 100Z" stroke="#FFF"/>
                <path d="M500.133 100L464.946 100C462.71 100 461.26 102.359 462.269 104.354L466.813 113.333L499.304 178.341C499.812 179.358 500.851 180 501.987 180H510.466C512.123 180 513.466 178.657 513.466 177V116.333C513.466 114.676 514.809 113.333 516.466 113.333H537.133C538.79 113.333 540.133 111.99 540.133 110.333V103C540.133 101.343 538.79 100 537.133 100H513.466H500.133Z" stroke="#FFF"/>
                <path d="M416.587 166.667C415.451 166.667 414.412 166.025 413.904 165.008L382.229 101.658C381.721 100.642 380.682 100 379.546 100H371.066C369.41 100 368.066 101.343 368.066 103V166.667V177C368.066 178.657 369.41 180 371.066 180H381.4H421.4H433.288C435.519 180 436.969 177.651 435.97 175.656L432.296 168.323C431.788 167.308 430.749 166.667 429.614 166.667H416.587Z" stroke="#FFF"/>
            </g>
        </svg>
    </div>
<?php endif; ?>
    <div id="mainContent" data-page="<?php echo isset($current_page)?$current_page:''; ?>">

        <header class="top-bar" id="topBar">
            <nav class="container-fluid">
                <div class="logo-wrapper">
                    <a href="<?php echo home_url(); ?>" class="logo">
                        <img src="<?php tu(); ?>/assets/images/logo.svg" >
                    </a>
                </div>
                <?php if($topStyle=='temas'): ?>
                    <?php get_template_part('blocks/_temas-nav'); ?> 
                <?php else: ?>
                    <div class="apoiadores">
                        <div>
                            <img src="<?php tu(); ?>/assets/images/publica.png">
                            <img src="<?php tu(); ?>/assets/images/cpt.png">
                        </div>
                    </div>
                <?php endif; ?>
                <div class="menu">
                    <div class="icons">
                        <?php if($topStyle=='temas'): ?>
                        <div class="search-wrapper">
                            <input type="text" class="search" list="buscamunicipio" onchange="mapApp.search(this)" onkeyup="mapApp.search(this)"> 
                            <datalist id="buscamunicipio"></datalist>
                        </div>
                        <div class="year-wrapper selectbox">
                            <input class="select-value" type="text" value="2020" onchange="mapApp.year=this.value;mapApp.filter();" id="yearValue">
                            <input class="check-drop" id="yearSelect" type="checkbox">
                            <label class="label-select" for="yearSelect" onclick="play('drop');">2020</label>
                            <ul class="drop-options">
                                <li class="select-option" data-value="2020">2020</li>
                                <li class="select-option" data-value="2019">2019</li>
                                <li class="select-option" data-value="2018">2018</li>
                                <li class="select-option" data-value="2017">2017</li>
                                <li class="select-option" data-value="2016">2016</li>
                                <li class="select-option" data-value="2015">2015</li>
                                <li class="select-option" data-value="2014">2014</li>
                                <li class="select-option" data-value="2013">2013</li>
                                <li class="select-option" data-value="2012">2012</li>
                                <li class="select-option" data-value="2011">2011</li>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <div class="btn-menu-wrapper">
                            <label for="btn-menu" class="btn-menu" onclick="play('click');"></label>
                        </div>
                    </div>
                    <input type="checkbox" id="btn-menu">
                    <div class="nav-menu">
                        <label for="btn-menu" class="btn-close" onclick="play('close');"></label>
                        <div class="lang">
                            <a href="<?php echo home_url(''); ?>" target="_self">PT</a>
                            <a href="<?php echo home_url('es'); ?>" target="_self">ES</a>
                            <a href="<?php echo home_url('en'); ?>" target="_self">EN</a>
                        </div>
                        <ul>
                            <?php get_template_part('blocks/_links-nav'); ?> 
                        </ul>
                    </div>

                </div>

            </nav>
        </header>