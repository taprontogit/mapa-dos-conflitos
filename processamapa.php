<?php


// Dividir por X, Ex. 3 mantém 1 terço dos pontos
$divisor = 1;
$decimais = 4;
$inicial = './assets/map/map.json';
$final = './assets/map/map.min.json';

try {
	
	$m = json_decode(file_get_contents($inicial));
	foreach ($m->features as $a=>$q) {
		if(isset($m->features[$a]->properties->name)) unset($m->features[$a]->properties->name);
		if(isset($m->features[$a]->properties->description)) unset($m->features[$a]->properties->description);
		foreach ($q->geometry->coordinates as $s=>$w){
			foreach ($w as $d=>$e){
				if(is_numeric($e[0])){
					if($d%$divisor && sizeof($w) > $divisor){
						unset($m->features[$a]->geometry->coordinates[$s][$d]);
					}else{
						foreach ($e as $g=>$t){
							$m->features[$a]->geometry->coordinates[$s][$d][$g] = round($t,$decimais);
						}
					}
					$m->features[$a]->geometry->coordinates[$s] = array_values($m->features[$a]->geometry->coordinates[$s]);
				}else{
					foreach ($e as $f=>$r){
						if($f%$divisor && sizeof($e) > $divisor){
							unset($m->features[$a]->geometry->coordinates[$s][$d][$f]);
						}else{
							foreach ($r as $g=>$t){
								$m->features[$a]->geometry->coordinates[$s][$d][$f][$g] = round($t,$decimais);
							}
						}
					}
					$m->features[$a]->geometry->coordinates[$s][$d] = array_values($m->features[$a]->geometry->coordinates[$s][$d]);
				}
			}
		}
	}
	$fp = fopen($final, 'w');
	fwrite($fp, json_encode($m));
	fclose($fp);

	echo 'Sucesso absoluto!';

} catch (Exception $e) {
	
	var_dump($e);
	
}